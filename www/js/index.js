/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


/**
 * Development playlist for this app:
 * - Spineshank
 * - Korn
 * - Godsmack
 * - Skindred
 * - Bob Marley
 * - Disturbed
 * - Bad Religion
 * - Imagine Dragons
 **/

/**
* Globals
**/

var monthList = ['januari','februari','maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];
var weekdays = ['Zondag','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'];
var reminderFrequenties = {
            "0.5": "elke 2 weken",
            "1": "elke maand",
            "1.5": "elke 1,5 maand",
            "2": "elke 2 maanden",
            "2.5": "elke 2,5 maand",
            "3": "elke 3 maanden",
            "6": "elke 6 maanden",
            "12": "elke jaar",
        }
//capsulating for the setup calls

var rest = {
    
createHeader:function(restcall, token)
{
  var weekdays = new Array(7);
  weekdays[0] = "Sun";
  weekdays[1] = "Mon";
  weekdays[2] = "Tue";
  weekdays[3] = "Wed";
  weekdays[4] = "Thu";
  weekdays[5] = "Fri";
  weekdays[6] = "Sat";
  
  var d = new Date();
  d.setHours(d.getHours()-2);
  
  var n = d.toString();
  var res = n.split(" ");
  
  var weekday = res[0];
  var month = res[1];
  var day = res[2];
  var year = res[3];
  var time = res[4];
  var timezone = 'GMT';
  
  var datum = weekday+', '+day+' '+month+' '+year+' '+time+' '+timezone;
  
  var apikey = '204b7822-1337-4375-a977-a1b2c3d4e5f6';
  var nonce = Math.floor(10000000 + Math.random() * 90000000);
  var secret = '20E8CD4ABD9447EE9D785C371B28CF26';
  
  var decriptedHash = nonce+':'+datum+':'+apikey;
  var hash = CryptoJS.HmacSHA256(decriptedHash, secret);
  var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
  
  var autorisation = 'hmac '+apikey+':'+nonce+':'+hashInBase64;
  
  switch (restcall)
  {
      case "login":
          $('#login-loader').show();

          rest.login(datum,autorisation);
          break;
      case "updateUserLogin":
          $('#updatelogin-loader').show();
          rest.updateUserLogin(datum,autorisation);
          break;
      case "securitycodeRequest":
          rest.securitycodeRequest(datum,autorisation,token);
          break;
      case "verifySecurityCode":
          $('#sessioncode-loader').show();
          rest.verifySecurityCode(datum,autorisation,token);
          break;
      case "updateVerifySecurityCode":
          $('#updatesessioncode-loader').show();
          rest.updateVerifySecurityCode(datum,autorisation,token);
          break;
      case "slaveLogin":
          rest.slaveLogin(datum,autorisation);
          break;
      case "createSlave":
          rest.createSlave(datum,autorisation,token);
          break;
      case "userinfo":
          rest.userinfo(datum,autorisation,token);
          break;
      case "createuser":
          $("#registration-loader").show();
          rest.createuser(datum,autorisation); 
          break;
      case "updateuser":
          rest.updateuser(datum,autorisation,token);
          break;
      case "getagendas":
          $('#agenda-popup-loader').show();
          rest.getagendas(datum,autorisation,token);
          break;
      case "getagendatext":
          rest.getagendatext(datum,autorisation,token);
          break;
      case "getavailabledates":
          $('#agenda-loader').show();
          rest.getavailabledates(datum,autorisation,token);
          break;
      case "getclosestdate":
          rest.getclosestdate(datum,autorisation,token);
          break;
      case "getnearesttimeblock":
          rest.getnearesttimeblock(datum,autorisation,token);
          break;
      case "getTimeSlots":
          $('#agenda-loader').show();
          rest.getTimeSlots(datum,autorisation,token);
          break;
      case "countAvailableSlots":
          rest.countAvailableSlots(datum,autorisation,token);
          break;
      case "createappointment":
          $('#afspraak-loader').show();
          rest.createappointment(datum,autorisation,token);
          break;
      case "getappointments":
          $('#agenda-popup-loader').show();
          rest.getappointments(datum,autorisation,token);
          break;
      case "showOpenAppointments":
          $('#dashboard-loader').show();
          rest.showOpenAppointments(datum,autorisation,token);
          break;
      case "getappointmentdetails":
          $('#appointment-details-loader').show();
          rest.getappointmentdetails(datum,autorisation,token);
          break;
      case "removeAppointment":
          rest.removeAppointment(datum,autorisation,token);
          break;
      case "getemployees":
          $('#question-popup-loader').show();
          rest.getemployees(datum,autorisation,token);
          break;
      case "geteconsulttext":
          rest.geteconsulttext(datum,autorisation,token);
          break;
      case "postconsult":
          rest.postconsult(datum,autorisation,token);
          break;
      case "consultoverview":
          $('#question-popup-loader').show();
          rest.consultoverview(datum,autorisation,token);
          break;
      case "consultdetails":
          $('#consult-loader').show();
          rest.consultdetails(datum,autorisation,token);
          break;
      case "getOpenQuestions":
          rest.getOpenQuestions(datum,autorisation,token);
          break;
      case "getmedicationoverview":
          $('#med-popup-loader').show();
          rest.getmedicationoverview(datum,autorisation,token);
          break;
      case "getmedicationrequestlist":
          $('#med-popup-loader').show();
          rest.getmedicationrequestlist(datum,autorisation,token);
          break;
      case "sendMedicationOrder":
          //$('#dashboard-loader').show();
          rest.sendMedicationOrder(datum,autorisation,token);
          break;
      case "getMedicationSettings":
          //$('#dashboard-loader').show();
          rest.getMedicationSettings(datum,autorisation,token);
          break;
      case "postreminder":
        rest.postreminder(datum,autorisation,token);
        break;
      case "deleteReminder":
        rest.deleteReminder(datum,autorisation,token);
        break;
      case "linkStatus":
        rest.linkStatus(datum,autorisation,token);
        break;

  }
},

linkStatus: function(datum, autorisation, token)
{
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+accountmanagement_users+'/linkstatuses',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      switch (data[0]["LinkStatus"])
      {
        case "LINKED":
          $( "#medication-section" ).removeClass( "disable" );
          $( "#medication-section .icon" ).removeClass( "disable" );
          $( "#medication-section .message" ).removeClass( "disable" );
          $( "#medication-section .chevron" ).removeClass( "disable" );

          $( "#comments-section" ).removeClass( "disable" );
          $( "#comments-section .icon" ).removeClass( "disable" );
          $( "#comments-section .message" ).removeClass( "disable" );
          $( "#comments-section .chevron" ).removeClass( "disable" );

          $( "#appointments-section" ).removeClass( "disable" );
          $( "#appointments-section .icon" ).removeClass( "disable" );
          $( "#appointments-section .message" ).removeClass( "disable" );
          $( "#appointments-section .chevron" ).removeClass( "disable" );
          
          $( "#medicationMenu" ).removeClass( "disablelinking" );
          $( "#questionsMenu" ).removeClass( "disablelinking" );
          $( "#appointmentsMenu" ).removeClass( "disablelinking" );
        break;
        case "NEW":
          navigator.notification.alert("Uw account is nog niet gekoppeld aan de patiëntservices van de praktijk. Uw aanvraag wordt zo spoedig mogelijkheid afgehandeld door de praktijk.", function(){}, "Let op", "OK");
        break;
        case "SENT":
          navigator.notification.alert("Uw account is nog niet gekoppeld aan de patiëntservices van de praktijk. Uw aanvraag is verzonden en wordt zo spoedig mogelijkheid afgehandeld door de praktijk.", function(){}, "Let op", "OK");
        break;
        default:
          alert(data[0]["LinkStatus"]);
          navigator.notification.alert("Uw account is nog niet volledig gekoppeld aan de patiëntservices van de praktijk.", function(){}, "Let op", "OK");
      }
    },
    error: function(jqXHR, textStatus)
    {
      //alert(JSON.stringify(jqXHR));
      switch (jqXHR["status"])
      {
          case 401:
          navigator.notification.alert("Onvoldoende rechten om de link status op te vragen", function(){}, "Helaas", "OK");
          break;
          case 500:
          navigator.notification.alert("Het is niet gelukt om de linkstatus te laden.", function(){}, "Helaas", "OK");
          break;
          default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om de link status te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
      $('#dashboard-loader').hide();
    }
  });
  $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
},

getMedicationSettings: function(datum, autorisation, token)
{
  $('#dashboard-loader').show();
   /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"HHRDoctor", extentie:"settings",postvars:"", instanceid:hhr, type:"GET", token:token},
    dataType:'json',
       success: function(data) {
      
        $("#MedicationOverviewText").empty();
        $("#OrderListText").empty();
      
        MedicationHistoryFilterMonth = data["MedicationHistoryFilterMonth"];
        MedicationOverviewText = data["MedicationOverviewText"];
        OrderListText = data["OrderListText"];
        DeliveryText = data["DeliveryText"];
        OrderSendOkText = data["OrderSendOkText"];
        EnableRemarks = data["EnableRemarks"];


      $("#MedicationOverviewText").append(MedicationOverviewText);
      $("#OrderListText").append(OrderListText+' '+DeliveryText);
      $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
      navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om uw medicatie overzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
     }
     });
*/
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+hhr+'/settings',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      $("#MedicationOverviewText").empty();
      $("#OrderListText").empty();
      
      MedicationHistoryFilterMonth = data["MedicationHistoryFilterMonth"];
      MedicationOverviewText = data["MedicationOverviewText"];
      OrderListText = data["OrderListText"];
      DeliveryText = data["DeliveryText"];
      OrderSendOkText = data["OrderSendOkText"];
      EnableRemarks = data["EnableRemarks"];

      $("#MedicationOverviewText").append(MedicationOverviewText);
      $("#OrderListText").append(OrderListText+' '+DeliveryText);
      $('#dashboard-loader').hide();
    },
    error: function(jqXHR, textStatus)
    {
      //alert(JSON.stringify(jqXHR["status"]));
      switch (jqXHR["status"])
      {
          case 401:
          navigator.notification.alert("Onvoldoende rechten om het medicatieoverzicht op te vragen", function(){}, "Helaas", "OK");
          break;
          case 500:
          navigator.notification.alert("Het is niet gelukt om uw medicatie overzicht te laden.", function(){}, "Helaas", "OK");
          break;
          default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om uw medicatie overzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }

      $('#dashboard-loader').hide();
    }
  });
},

deleteReminder: function(datum, autorisation, token)
{
  var post = {
            Hpk: deletehpk,
            Prk: deleteprk
        }
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"HHRDoctor", extentie:"reminders",postvars:post, instanceid:hhr, type:"DELETE", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 200:
            navigator.notification.alert("De herinnering is verwijderd voor "+deleteName+".", function(){}, "Herinnering", "OK");
            rest.getToken("getmedicationoverview");
            statistics.register("reminderremoved","200");
            break;
            case 400:
            navigator.notification.alert("De herinnering voor "+deleteName+" kon niet verwijderd worden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","400");
            break;
            case 401:
            navigator.notification.alert("Onvoldoende rechten om de herinnering voor "+deleteName+" te verwijderen.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","401");
            break;
            case 404:
            navigator.notification.alert("De herinnering voor "+deleteName+" kon niet worden gevonden om te verwijderen.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","404");
            break;
            case 500:
            navigator.notification.alert("Het is niet gelukt om uw herinnering voor "+deleteName+" te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","500");
            break;
            default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. HHet is niet gelukt om uw herinnering voor "+deleteName+" te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("reminderremoved","default");
        }

      },
      error: function(jqXHR, textStatus)
      {
        switch (jqXHR["status"])
        {
            case 200:
            navigator.notification.alert("De herinnering is verwijderd voor "+deleteName+".", function(){}, "Herinnering", "OK");
            rest.getToken("getmedicationoverview");
            statistics.register("reminderremoved","200");
            break;
            case 400:
            navigator.notification.alert("De herinnering voor "+deleteName+" kon niet verwijderd worden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","400");
            break;
            case 401:
            navigator.notification.alert("Onvoldoende rechten om de herinnering voor "+deleteName+" te verwijderen.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","401");
            break;
            case 404:
            navigator.notification.alert("De herinnering voor "+deleteName+" kon niet worden gevonden om te verwijderen.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","404");
            break;
            case 500:
            navigator.notification.alert("Het is niet gelukt om uw herinnering voor "+deleteName+" te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("reminderremoved","500");
            break;
            default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. HHet is niet gelukt om uw herinnering voor "+deleteName+" te verwijderen. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
              statistics.register("reminderremoved","default");
        }
      }
    });
  */
  
  $.ajax(
   {
   url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+hhr+'/reminders',
   type: 'DELETE',
   headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
   dataType:'json',
   data: JSON.stringify(post),
   success: function(data)
   {
      navigator.notification.alert("De herinnering is verwijderd voor "+deleteName+".", function(){}, "Herinnering", "OK");
      rest.getToken("getmedicationoverview");
      statistics.register("reminderremoved","200");
   },
   error: function(jqXHR, textStatus)
   {
      switch (jqXHR["status"])
      {
        case 200:
        navigator.notification.alert("De herinnering is verwijderd voor "+deleteName+".", function(){}, "Herinnering", "OK");
        rest.getToken("getmedicationoverview");
        statistics.register("reminderremoved","200");
        break;
        case 400:
        navigator.notification.alert("De herinnering voor "+deleteName+" kon niet verwijderd worden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
        statistics.register("reminderremoved","400");
        break;
        case 401:
        navigator.notification.alert("Onvoldoende rechten om de herinnering voor "+deleteName+" te verwijderen.", function(){}, "Helaas", "OK");
        statistics.register("reminderremoved","401");
        break;
        case 404:
        navigator.notification.alert("De herinnering voor "+deleteName+" kon niet worden gevonden om te verwijderen.", function(){}, "Helaas", "OK");
        statistics.register("reminderremoved","404");
        break;
        case 500:
        navigator.notification.alert("Het is niet gelukt om uw herinnering voor "+deleteName+" te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        statistics.register("reminderremoved","500");
        break;
        default:
          navigator.notification.alert("Er is een onbekende fout opgetreden. HHet is niet gelukt om uw herinnering voor "+deleteName+" te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("reminderremoved","default");
      }
      $('#reminderpage-loader').hide();
   }
   });
},

postreminder: function(datum, autorisation, token)
{

  $('#reminderpage-loader').show();
  var hpk = document.getElementById('reminder_hpk').value;
  var prk = document.getElementById('reminder_prk').value;
  var reminderdate = document.getElementById('reminderDate').value;
  var Interval = document.getElementById('repeatInterval').value;
  var RepeatType = document.getElementById('repeattype').value;
  var medName = document.getElementById('reminder_medicationName').value;
  
  if(reminderdate == "")
  {
    navigator.notification.alert("U dient aan te geven op welke datum u een bestelherinnering wenst te ontvangen.", function(){}, "Herinnering", "OK");  
    $('#reminderpage-loader').hide();
  }
  else if (RepeatType == "")
  {
    navigator.notification.alert("U dient aan te geven of u eenmalig of terugkerend een bestelherinnering wenst te onvangen", function(){}, "Herinnering", "OK");  
    $('#reminderpage-loader').hide();
  }
  else if (RepeatType == "1" && Interval == "")
  {
    navigator.notification.alert("U dient aan te geven in welke frequentie u de terugkerende herinnering wenst te ontvangen.", function(){}, "Herinnering", "OK");  
    $('#reminderpage-loader').hide();
  }
  else
  {
    var post = {
            Date: reminderdate,
            RepeatType: parseInt(RepeatType),
            Interval: Interval,
            IsViewed: false,
            Hpk: hpk,
            Prk: prk
        }
    /*
    $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"HHRDoctor", extentie:"reminders",postvars:post, instanceid:hhr, type:"POST", token:token},
      dataType:'json',
      success: function(data) {
            navigator.notification.alert("De herinnering is ingesteld voor "+medName+".", function(){}, "Herinnering", "OK");
              rest.getToken("getmedicationoverview");
              statistics.register("setreminder","200");

      },
      error: function(jqXHR, textStatus)
      {
        switch (jqXHR["status"])
        {
            case 200:
              navigator.notification.alert("De herinnering is ingesteld voor "+medName+".", function(){}, "Herinnering", "OK");
              rest.getToken("getmedicationoverview");
              statistics.register("setreminder","200");
            break;
            case 400:
              navigator.notification.alert("De herinnering voor "+medName+" kon niet ingesteld worden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
            statistics.register("setreminder","400");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de herinnering voor "+medName+" in te stellen", function(){}, "Helaas", "OK");
              statistics.register("setreminder","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om uw herinnering voor "+medName+" in te stellen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("setreminder","500");
            break;
            default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. HHet is niet gelukt om uw herinnering voor "+medName+" in te stellen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("setreminder","default");
        }
      }
    });
*/

    $.ajax(
    {
      url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+hhr+'/reminders',
      type: 'POST',
      headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
      dataType:'json',
      data: JSON.stringify(post),
      success: function(data)
      {
          navigator.notification.alert("De herinnering is ingesteld voor medicatie x.", function(){}, "Herinnering", "OK");
          rest.getToken("getmedicationoverview");
          statistics.register("setreminder","200");
          $('#reminderpage-loader').hide();
      },
      error: function(jqXHR, textStatus)
      {
        //alert(JSON.stringify(jqXHR["status"]));
        switch (jqXHR["status"])
        {
            case 200:
              navigator.notification.alert("De herinnering is ingesteld voor "+medName+".", function(){}, "Herinnering", "OK");
              rest.getToken("getmedicationoverview");
              statistics.register("setreminder","200");
            break;
            case 400:
              navigator.notification.alert("De herinnering voor "+medName+" kon niet ingesteld worden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
            statistics.register("setreminder","400");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de herinnering voor "+medName+" in te stellen", function(){}, "Helaas", "OK");
              statistics.register("setreminder","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om uw herinnering voor "+medName+" in te stellen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("setreminder","500");
            break;
            default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. HHet is niet gelukt om uw herinnering voor "+medName+" in te stellen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("setreminder","default");
        }
        $('#reminderpage-loader').hide();
      }
    });
  }
},

sendMedicationOrder: function(datum,autorisation,token)
{
  $('#orderpage-loader').show();
  var jsonStr = '{"MedicationsToOrder":[]}';
  var query = "SELECT * FROM tblRequestList";
            
  db.transaction(function(tx)
  {
      tx.executeSql(query,[],function(tx, result)
      {
        dataset = result.rows;
        if(dataset.length == 0)
        {
            navigator.notification.alert("Geen medicatie gevonden", function(){}, "Melding", "OK");
        }
        else
        {
            for (var i = 0, item =null; i < dataset.length; i++)
            {
              item = dataset.item(i);
                  
              var obj = JSON.parse(jsonStr);
              obj['MedicationsToOrder'].push({"MedicationId":item['listID'],"Remark":""});
              jsonStr = JSON.stringify(obj);
            }
          //rest.postMedicationOrder(datum,autorisation,token,obj); //PLESK solution
          rest.postMedicationOrder(datum,autorisation,token,jsonStr);
          //request.requestList();
          //$.mobile.changePage("#medicationrequest-page", { transition: "slide", changeHash: false });
      }
      });
  });

},

postMedicationOrder: function(datum,autorisation,token,jsonStr)
{
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"HHRDoctor", extentie:"orders",postvars:jsonStr, instanceid:hhr, type:"POST", token:token},
     dataType:'json',
     success: function(data) {
      switch (data)
        {
            case 200:
              navigator.notification.alert(""+OrderSendOkText+"", function(){}, "Gelukt", "OK");
              $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
              request.clearRequestList();
              statistics.register("ordermedication","200");
            break;
            case 400:
              navigator.notification.alert("Uw bestelling kon niet worden verzonden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
              statistics.register("ordermedication","400");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de bestelling te verzenden", function(){}, "Helaas", "OK");
              statistics.register("ordermedication","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om uw bestelling te verzenden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("ordermedication","500");
            break;
            default:
              navigator.notification.alert(""+OrderSendOkText+"", function(){}, "Gelukt", "OK");
              $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
              request.clearRequestList();
              statistics.register("ordermedication","200");
        }  
        $('#orderpage-loader').hide();
      /*alert(JSON.stringify(data));
      navigator.notification.alert(""+OrderSendOkText+"", function(){}, "Gelukt", "OK");
        $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
        request.clearRequestList();
        $('#orderpage-loader').hide();
        statistics.register("ordermedication","200"); */
        /*

     },
     error: function(jqXHR, textStatus)
     {
        switch (jqXHR["status"])
        {
            case 200:
              navigator.notification.alert(""+OrderSendOkText+"", function(){}, "Gelukt", "OK");
              $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
              request.clearRequestList();
              statistics.register("ordermedication","200");
            break;
            case 400:
              navigator.notification.alert("Uw bestelling kon niet worden verzonden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
              statistics.register("ordermedication","400");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de bestelling te verzenden", function(){}, "Helaas", "OK");
              statistics.register("ordermedication","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om uw bestelling te verzenden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("ordermedication","500");
            break;
            default:
              navigator.notification.alert("Het is niet gelukt om uw bestelling te verzenden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
              statistics.register("ordermedication","connectionerror");
        }  
        $('#orderpage-loader').hide();
     }
     });
  */
  
  $.ajax(
   {
   url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+hhr+'/orders',
   type: 'POST',
   headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
   dataType:'json',
   data: jsonStr,
   success: function(data)
   {
        navigator.notification.alert(""+OrderSendOkText+"", function(){}, "Gelukt", "OK");
        $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
        request.clearRequestList();
        $('#orderpage-loader').hide();
        statistics.register("ordermedication","200");
   },
   error: function(jqXHR, textStatus)
   {
      switch (jqXHR["status"])
      {
          case 200:
            navigator.notification.alert(""+OrderSendOkText+"", function(){}, "Gelukt", "OK");
            $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
            request.clearRequestList();
            statistics.register("ordermedication","200");
          break;
          case 400:
            navigator.notification.alert("Uw bestelling kon niet worden verzonden, omdat er gegevens ontbreken.", function(){}, "Helaas", "OK");
            statistics.register("ordermedication","400");
          break;
          case 401:
            navigator.notification.alert("Onvoldoende rechten om de bestelling te verzenden", function(){}, "Helaas", "OK");
            statistics.register("ordermedication","401");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om uw bestelling te verzenden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("ordermedication","500");
          break;
          default:
            navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om uw bestelling te verzenden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("ordermedication","default");
      }
      $('#orderpage-loader').hide();
   }
  });
},

getmedicationrequestlist: function(datum,autorisation,token)
{
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"HHRDoctor", extentie:"medications",postvars:"", instanceid:hhr, type:"GET", token:token},
    dataType:'json',
       success: function(data) {
        var emptyTable = "DELETE FROM tblMedication";
        var insertMedication = "INSERT INTO tblMedication (MedicationId, MedicationName, AmountPerRepetition, Unity, UsageText, PrescriberName, StartDate, EndDate, AllowToOrder, Prk, Hpk, IsHistory) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        db.transaction(function(tx, results)
        {
          tx.executeSql(emptyTable,[],dbase.succesCB,dbase.errorCB);
          for (var i = 0, item =null; i < data.length; i++)
          {
              item = data[i];
              tx.executeSql(insertMedication,[item['MedicationId'], item['MedicationName'], item['AmountPerRepetition'], item['Unity'], item['UsageText'], item['PrescriberName'], item['StartDate'], item['EndDate'], item['AllowToOrder'], item['Prk'], item['Hpk'],item['IsHistory']],dbase.succesCB,dbase.errorCB);   
          }
        });
        request.overview();  
        statistics.register("getmedicationorderlist","200");
        

     },
     error: function(jqXHR, textStatus)
     {
      navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om de lijst met te bestellen medicatie te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      statistics.register("getmedicationorderlist","default");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+hhr+'/medications',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      //alert(JSON.stringify(data));
      var emptyTable = "DELETE FROM tblMedication";
      var insertMedication = "INSERT INTO tblMedication (MedicationId, MedicationName, AmountPerRepetition, Unity, UsageText, PrescriberName, StartDate, EndDate, AllowToOrder, Prk, Hpk, IsHistory) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      db.transaction(function(tx, results)
      {
        tx.executeSql(emptyTable,[],dbase.succesCB,dbase.errorCB);
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            tx.executeSql(insertMedication,[item['MedicationId'], item['MedicationName'], item['AmountPerRepetition'], item['Unity'], item['UsageText'], item['PrescriberName'], item['StartDate'], item['EndDate'], item['AllowToOrder'], item['Prk'], item['Hpk'],item['IsHistory']],dbase.succesCB,dbase.errorCB);   
        }
      });
      request.overview();  
      statistics.register("getmedicationorderlist","200");
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
          case 401:
            navigator.notification.alert("Onvoldoende rechten om de lijst met te bestellen medicatie op te halen. Probeer opnieuw in te loggen.", function(){}, "Helaas", "OK");
            statistics.register("getmedicationorderlist","401");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om de lijst met bestelbare medicatie te laden.", function(){}, "Helaas", "OK");
            statistics.register("getmedicationorderlist","500");
          break;
          default:
            navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om de lijst met te bestellen medicatie te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("getmedicationorderlist","default");
      }
      $('#med-popup-loader').hide();
    }
  });
},

getmedicationoverview: function(datum,autorisation,token)
{
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"HHRDoctor", extentie:"medications",postvars:"", instanceid:hhr, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om het medicatieoverzicht op te vragen", function(){}, "Helaas", "OK");
              statistics.register("getmedicationoverview","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om uw medicatie overzicht te laden.", function(){}, "Helaas", "OK");
              statistics.register("getmedicationoverview","500");
            break;
            default:
              medication.overview(data);
              $('#dashboard-loader').hide();
              statistics.register("getmedicationoverview","200");
        }  


        $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om uw medicatie overzicht te laden.", function(){}, "Verbindingsfout", "OK");
        statistics.register("getmedicationoverview","500");
        $('#dashboard-loader').hide();
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/HHRDoctor/'+hhr+'/medications',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      //alert(JSON.stringify(data));
      medication.overview(data);
      $('#med-popup-loader').hide();
      statistics.register("getmedicationoverview","200");
    },
    error: function(jqXHR, textStatus)
    {
      //alert(JSON.stringify(jqXHR["status"]));
      switch (jqXHR["status"])
      {
          case 401:
            navigator.notification.alert("Onvoldoende rechten om het medicatieoverzicht op te vragen", function(){}, "Helaas", "OK");
            statistics.register("getmedicationoverview","401");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om uw medicatie overzicht te laden.", function(){}, "Helaas", "OK");
            statistics.register("getmedicationoverview","500");
          break;
          default:
            navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om uw medicatie overzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("getmedicationoverview","default");
      }
      $('#med-popup-loader').hide();
    }
  });
},

geteconsulttext: function(datum,autorisation,token)
{
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"EConsult", extentie:"settings",postvars:"", instanceid:econsult, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de uitlegteksten te laden", function(){}, "Helaas", "OK");
              
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de uitlegteksten te laden.", function(){}, "Helaas", "OK");
              
            break;
            default:
              $.mobile.changePage("#questionselectemployee_page", { transition: "slide", changeHash: false });
              $('#dashboard-loader').hide();
        }  


        $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de uitlegteksten te laden.", function(){}, "Verbindingsfout", "OK");
              
              $('#dashboard-loader').hide();
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/EConsult/'+econsult+'/settings',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      //alert(JSON.stringify(data));
      $.mobile.changePage("#questionselectemployee_page", { transition: "slide", changeHash: true });
      $('#question-popup-loader').hide(); 
    },
    error: function(jqXHR, textStatus)
    {
      navigator.notification.alert("Het is niet gelukt om de begeleidende teksten te laden.", function(){}, "Helaas", "OK");
      $('#question-popup-loader').hide(); 
    }
  });
},
    
getemployees: function(datum,autorisation,token)
{
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"EConsult", extentie:"employees",postvars:"", instanceid:econsult, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten. De medewerkers konden niet worden geladen.", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("het is niet gelukt om het overzicht van beschikbare medewerkers te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              consult.showemployees(data);
              rest.getToken('geteconsulttext');
        }  
        $('#dashboard-loader').hide();
      },
      error: function(jqXHR, textStatus)
      {
        navigator.notification.alert("het is niet gelukt om het overzicht van beschikbare medewerkers te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        $('#dashboard-loader').hide();
      }
  });
*/

  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/EConsult/'+econsult+'/employees',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
        consult.showemployees(data);
        rest.getToken('geteconsulttext');
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Uw account is waarschijnlijk nog niet gekoppeld. De medewerkers konden niet worden geladen.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("het is niet gelukt om het overzicht van beschikbare medewerkers te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de beschikbare medewerkers op te halen.", function(){}, "Helaas", "OK");
      } 
      $('#question-popup-loader').hide(); 
    }
  });
},
    
postconsult: function(datum,autorisation,token)
{
  var employee = document.getElementById('consultemployee').value;
  var employeeid = document.getElementById('consultemployeeid').value;
  var subject = document.getElementById('consultsubject').value;
  var toelichting = document.getElementById('consultToelichtingArea').value;
  
  var d = new Date(),
  month = '' + (d.getMonth() + 1),
  day = '' + d.getDate(),
  year = d.getFullYear();
  
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  
  var requestdate = year+'-'+month+'-'+day+'T00:00:00';
  
  var employee_arr = {
      EmployeeId: employeeid,
      Name: employee
  }
  
  var post = {
      ConsultId: null,
      RequestDate: requestdate,
      ResponseDate: '0001-01-01T09:00:00',
      Subject: subject,
      Text: toelichting,
      Response: null,
      Status: 0,
      Unread: false,
      Employee: employee_arr
  }
  /*
  $.ajax({
    url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
    type: 'POST',
    data: { restcall:"EConsult", extentie:"consults",postvars:post, instanceid:econsult, type:"POST", token:token},
    dataType:'json',
    success: function(data) {
      switch (data)
      {
        case 400:
          navigator.notification.alert("Er ontbreken gegevens. Uw vraag kon niet verzonden worden naar de praktijk.", function(){}, "Helaas", "OK");
          statistics.register("postconsult","400");
        break;
        case 401:
          navigator.notification.alert("Uw account is waarschijnlijk nog niet gekoppeld. Uw vraag kon niet verzonden worden naar de praktijk.", function(){}, "Helaas", "OK");
          statistics.register("postconsult","401");
        break;
        case 500:
          navigator.notification.alert("Er is een onverwachte fout opgetreden bij het versturen van uw vraag. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("postconsult","500");
        break;
        default:
          $("#sendconsult-loader").hide();
          navigator.notification.alert("Uw vraag aan "+employee+" is verzonden.", function(){}, "Succes", "OK");
          rest.getToken("getOpenQuestions");
          rest.getToken("showOpenAppointments");
          $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
          statistics.register("postconsult","200");
      } 
      $("#sendconsult-loader").hide();

    },
    error: function(jqXHR, textStatus)
    {
      navigator.notification.alert("Er is een onverwachte fout opgetreden bij het versturen van uw vraag. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
      statistics.register("postconsult","connectionerror");
      $("#sendconsult-loader").hide();
    }
  });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/EConsult/'+econsult+'/consults',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
        $("#sendconsult-loader").hide();
        navigator.notification.alert("Uw vraag aan "+employee+" is verzonden.", function(){}, "Succes", "OK");
        rest.getToken("getOpenQuestions");
        rest.getToken("showOpenAppointments");
        $.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
        statistics.register("postconsult","200");
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 400:
          navigator.notification.alert("Er ontbreken gegevens. Uw vraag kon niet verzonden worden naar de praktijk.", function(){}, "Helaas", "OK");
          statistics.register("postconsult","400");
        break;
        case 401:
          navigator.notification.alert("Uw account is waarschijnlijk nog niet gekoppeld. Uw vraag kon niet verzonden worden naar de praktijk.", function(){}, "Helaas", "OK");
          statistics.register("postconsult","401");
        break;
        case 500:
          navigator.notification.alert("Er is een onverwachte fout opgetreden bij het versturen van uw vraag. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("postconsult","500");
        break;
        default:
          navigator.notification.alert("Er is een onverwachte fout opgetreden bij het versturen van uw vraag. ", function(){}, "Helaas", "OK");
          statistics.register("postconsult","default");
      } 
      $("#sendconsult-loader").hide();
    }
  });
},
    
consultoverview: function(datum,autorisation,token)
{
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"EConsult", extentie:"consults",postvars:"", instanceid:econsult, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten. Uw consult overzicht kon niet worden geladen.", function(){}, "Helaas", "OK");
              statistics.register("getconsults","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om uw consultoverzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("getconsults","500");
            break;
            default:
              consult.overview(data);
              statistics.register("getconsults","200");
        }  

        $('#dashboard-loader').hide();
      },
      error: function(jqXHR, textStatus)
      {
        navigator.notification.alert("Het is niet gelukt om uw consultoverzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        $('#dashboard-loader').hide();
        statistics.register("getconsults","connectionerror");
      }
  });
*/
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/EConsult/'+econsult+'/consults',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      consult.overview(data);

      statistics.register("getconsults","200");
    },
    error: function(jqXHR, textStatus)
    {
      //alert(jqXHR["status"]);
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Uw account is waarschijnlijk nog niet gekoppeld. Uw consult overzicht kon niet worden geladen.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om uw consultoverzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om uw consultoverzicht te laden.", function(){}, "Helaas", "OK");
      } 
      $('#question-popup-loader').hide();
    }
  });
},

consultdetails: function(datum,autorisation,token)
{
  var id = document.getElementById('retrieveID').value;
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"EConsult", extentie:"consults/"+id,postvars:"", instanceid:econsult, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
          case 401:
            navigator.notification.alert("Onvoldoende rechten. Uw vraag kon niet worden geladen.", function(){}, "Helaas", "OK");
            statistics.register("consultdetails","401");
          break;
          case 404:
            navigator.notification.alert("Uw vraag kon niet worden gevonden in het systeem.", function(){}, "Helaas", "OK");
            statistics.register("consultdetails","404");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om uw vraag te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("consultdetails","500");
          break;
          default:
            consult.details(data);
            statistics.register("consultdetails","200");
        }
        $('#consult-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om uw vraag te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        statistics.register("consultdetails","connectionerror");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/EConsult/'+econsult+'/consults/'+id,
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
        consult.details(data);
        statistics.register("consultdetails","200");
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Uw account is waarschijnlijk nog niet gekoppeld. Uw vraag kon niet worden geladen.", function(){}, "Helaas", "OK");
          statistics.register("consultdetails","401");
        break;
        case 404:
          navigator.notification.alert("Uw vraag kon niet worden gevonden in het systeem.", function(){}, "Helaas", "OK");
          statistics.register("consultdetails","404");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om uw vraag te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("consultdetails","500");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om uw vraag te laden.", function(){}, "Helaas", "OK");
          statistics.register("consultdetails","default");
      }
      $('#consult-loader').hide();
    }
  });
},

getOpenQuestions: function(datum,autorisation,token)
{
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"EConsult", extentie:"consults",postvars:"", instanceid:econsult, type:"GET", token:token},
    dataType:'json',
       success: function(data) {
        //alert(JSON.stringify(data));
        dashboard.consults(data);
        

     },
     error: function(jqXHR, textStatus)
     {
      navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om uw medicatie overzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/EConsult/'+econsult+'/consults',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
        dashboard.consults(data);
    },
    error: function(jqXHR, textStatus)
    {
        //navigator.notification.alert("Het is niet gelukt om uw consultoverzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
    }
  });
},

updateUserLogin:function(datum,autorisation)
{
  var username = document.getElementById('updateusername');
  var password = document.getElementById('updatepassword');
 
  var post = {
    UserName: username.value,
    UserPassword: password.value
  }
  /*
  $.ajax({
    url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
    type: 'POST',
    data: { restcall:"Sessions", extentie:"sessions",postvars:post, instanceid:accountmanagement_sessions, type:"POST", token:""},
    dataType:'json',
    success: function(data) {
      if(data['StatusCode'] == "OK")
      {
          account.storeToken(data['Token']);
          rest.getToken("updateuser");
          $('#updatelogin-loader').hide();
      }
      if(data['StatusCode'] == "Unauthorized")
      {
        if(data['Token'] != null)
        {
            account.storeToken(data["Token"]);
            navigator.notification.alert("Er is een verificatiecode naar u verzonden. Vul deze in om uw accountgegevens aan te passen.", function(){}, "Verzonden", "OK");  
            $.mobile.changePage("#updateverifycode-page", { transition: "slide", changeHash: false });
            $('#updatelogin-loader').hide();
            
        }
        else
        {
          $('#updatelogin-loader').hide();
          navigator.notification.alert("Het door u opgegeven gebruikersnaam en wachwoord is niet bekend. Probeer het nogmaals.", function(){}, "Helaas", "OK");  
        }
      }
      $('#login-loader').hide();
    },
    error: function(jqXHR, textStatus)
    {
        navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");  
    }
  });

  */
  
  var post = {
    UserName: username.value,
    UserPassword: password.value
  }
  //alert(accountmanagement_sessions);
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Sessions/'+accountmanagement_sessions+'/sessions',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
     if(data['Token'])
     {
          account.storeToken(data['Token']);
          rest.getToken("updateuser");
          $('#updatelogin-loader').hide();
     }
    },
    error: function(jqXHR, textStatus)
    {
      if(jqXHR["status"] == '401')
      {
          if(jqXHR["responseJSON"]["Token"] == null)
          {
            $('#updatelogin-loader').hide();
            navigator.notification.alert("Het door u opgegeven gebruikersnaam en wachwoord is niet bekend. Probeer het nogmaals.", function(){}, "Helaas", "OK");  

          }
          else
          {
            account.storeToken(jqXHR["responseJSON"]["Token"]);
            navigator.notification.alert("Er is een verificatiecode naar u verzonden. Vul deze in om uw accountgegevens aan te passen.", function(){}, "Verzonden", "OK");  
            $.mobile.changePage("#updateverifycode-page", { transition: "slide", changeHash: false });
            $('#updatelogin-loader').hide();
          }
      }
      else
      {
          $('#updatelogin-loader').hide();
          navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");  
      }
    }
  });
},

updateuser: function(datum,autorisation,token)
{
  var username = document.getElementById("UserName").value;
  var gender = document.getElementById("update_gender").value;
  var initials = document.getElementById("update_initials").value;
  var firstname = document.getElementById("update_firstname").value;
  var infix = document.getElementById("update_infix").value;
  var lastname = document.getElementById("update_lastname").value;
  var birthdate = document.getElementById("update_birthdate").value;
  var street = document.getElementById("update_street").value;
  var housenumber = document.getElementById("update_housenumber").value;
  var postalcode = document.getElementById("update_postalcode").value;
  var city = document.getElementById("update_city").value;
  var mobilenumber = document.getElementById("update_mobilenumber").value;
  var email = document.getElementById("update_email").value;

  var address = new Array();

  address[0] = {
  Street: street,
  HouseNumber: housenumber,
  City: city,
  PostalCode: postalcode,
  Type:'Default'
  }

  var post = {
  UserName: username,
  Email: email,
  Gender: gender,
  MobileNumber: mobilenumber,
  Birthdate: birthdate,
  FirstName: firstname,
  LastName: lastname,
  Infix: infix,
  Initials: initials,
  Device: 'Mobile',
  AddressList:address
  }
  /*
  $.ajax({
    url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
    type: 'POST',
    data: { restcall:"Users", extentie:"users",postvars:post, instanceid:accountmanagement_users, type:"PUT", token:token},
    dataType:'json',
    success: function(data) {
      switch (data)
      {
        case 401:
          navigator.notification.alert("Uw heeft onvoldoende rechten om de gegevens bij te werken.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","401");
        break;
        case 404:
          navigator.notification.alert("Het account om bij te werken kon niet worden gevonden.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","404");
        break;
        case 500:
          navigator.notification.alert("Er is een onverwachte fout opgetreden. Uw gegevens konden niet worden bijgewerkt.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","500");
        break;
        default:
          $.mobile.changePage("#settings-page", { transition: "slidedown", changeHash: false });
          navigator.notification.alert("Uw gegevens zijn bijgewerkt.", function(){}, "Succes", "OK");
          statistics.register("accountupdate","200");
      }
      $("#usersettings-loader").hide();
      $('#updatesessioncode-loader').hide();
    },
    error: function(jqXHR, textStatus)
    {
        //alert(JSON.stringify(jqXHR));
        $("#usersettings-loader").hide();
    }
  });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Users/'+accountmanagement_users+'/users',
    type: 'PUT',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
      $("#usersettings-loader").hide();
      $.mobile.changePage("#settings-page", { transition: "slidedown", changeHash: false });
      navigator.notification.alert("Uw gegevens zijn bijgewerkt.", function(){}, "Succes", "OK");
      statistics.register("accountupdate","200");
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Uw heeft onvoldoende rechten om de gegevens bij te werken.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","401");
        break;
        case 404:
          navigator.notification.alert("Het account om bij te werken kon niet worden gevonden.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","404");
        break;
        case 500:
          navigator.notification.alert("Er is een onverwachte fout opgetreden. Uw gegevens konden niet worden bijgewerkt.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","500");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om uw gegevens bij te werken. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("accountupdate","default");
      }
      $("#usersettings-loader").hide();
    }
  });
},
    
createuser: function(datum,autorisation)
{
  var gender = document.getElementById("register_gender").value;
  var initials = document.getElementById("register_initials").value;
  var firstname = document.getElementById("register_firstname").value;
  var infix = document.getElementById("register_infix").value;
  var lastname = document.getElementById("register_lastname").value;
  var birthdate = document.getElementById("register_birthdate").value;
  var street = document.getElementById("register_street").value;
  var housenumber = document.getElementById("register_housenumber").value;
  var postalcode = document.getElementById("register_postalcode").value;
  var city = document.getElementById("register_city").value;
  var mobilenumber = document.getElementById("register_mobilenumber").value;
  var email = document.getElementById("register_email").value;
  var password = document.getElementById("register_password").value;


  var address = new Array();

  address[0] = {
  Street: street,
  HouseNumber: housenumber,
  City: city,
  PostalCode: postalcode,
  Type:'Default'
  }

  var post = {
  BSN: '',
  AccountId: 0,
  UserName: email,
  Email: email,
  Password: password,
  Gender: gender,
  MobileNumber: mobilenumber,
  Birthdate: birthdate,
  FullName: '',
  FirstName: firstname,
  LastName: lastname,
  Infix: infix,
  Initials: initials,
  Device: 'Mobile',
  AddressList:address
  }
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"Users", extentie:"users",postvars:post, instanceid:accountmanagement_users, type:"POST", token:""},
      dataType:'json',
       success: function(data) {
        //alert(JSON.stringify(data));
        switch (data)
        {
            case 409:
              navigator.notification.alert("Er is al een gebruikersaccount geregistreerd met de door u opgegeven gegevens.", function(){}, "Helaas", "OK");
              statistics.register("createuser","409");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de registratie te voltooien", function(){}, "Helaas", "OK");
              statistics.register("createuser","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om u registratie te voltooien. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("createuser","500");
            break;
            default:
              navigator.notification.alert("Uw account is succesvol aangemaakt. Let op: het kan enige dagen duren voordat uw account is gekoppeld aan de patientservices.", function(){}, "Gelukt", "OK");
              $.mobile.changePage("#login-page", { transition: "fade", changeHash: false });
              statistics.register("createuser","200");
        }

     },
     error: function(jqXHR, textStatus)
     {
         navigator.notification.alert("Het is niet gelukt om u registratie te voltooien. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
              statistics.register("createuser","connectionerror");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Users/'+accountmanagement_users+'/users',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
        //alert(JSON.stringify(data));
        $("#registration-loader").hide();
        navigator.notification.alert("Uw account is succesvol aangemaakt. Let op: het kan enige dagen duren voordat uw account is gekoppeld aan de patientservices.", function(){}, "Gelukt", "OK");
        $.mobile.changePage("#login-page", { transition: "fade", changeHash: false });
        statistics.register("createuser","200");

    },
    error: function(jqXHR, textStatus)
    {
      //alert(JSON.stringify(jqXHR["status"]));
      switch (jqXHR["status"])
      {
          case 409:
            navigator.notification.alert("Er is al een gebruikersaccount geregistreerd met de door u opgegeven gegevens.", function(){}, "Helaas", "OK");
            statistics.register("createuser","409");
            $("#registration-loader").hide();
          break;
          case 401:
            navigator.notification.alert("Onvoldoende rechten om de registratie te voltooien", function(){}, "Helaas", "OK");
            statistics.register("createuser","401");
            $("#registration-loader").hide();
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om u registratie te voltooien. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("createuser","500");
            $("#registration-loader").hide();
          break;
          default:
            navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om u registratie te voltooien. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("createuser","default");
            $("#registration-loader").hide();

      }

    }
  });
},
    
userinfo:function(datum,autorisation,token)
{
  var username = document.getElementById('username');
  var user = username.value.split("@");
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"Users", extentie:"users/username",postvars:"", instanceid:accountmanagement_users, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de accountgegevens te laden.", function(){}, "Helaas", "OK");
            break;
            case 404:
              navigator.notification.alert("Het opgegeven account kon niet worden gevonden.", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de accountgegevens te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              account.showuserinfo(data);
        }  


        $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de accountgegevens te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        
     }
});
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Users/'+accountmanagement_users+'/users/'+user[0],
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      account.showuserinfo(data);
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 401:
        navigator.notification.alert("Onvoldoende rechten om de accountgegevens te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
        navigator.notification.alert("Het opgegeven account kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
        navigator.notification.alert("Het is niet gelukt om de accountgegevens te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de accountgegevens te laden.", function(){}, "Helaas", "OK");
      }
    }
  });
},

createSlave:function(datum,autorisation,token)
{
  
  var query = "SELECT * FROM tblPractice";

  db.transaction(function(tx)
  {
    tx.executeSql(query,[],function(tx, result)
    {
      dataset = result.rows;
      if(dataset.length == 0)
      {
        navigator.notification.alert("Er zijn geen geldige praktijkgegevens gevonden. Sluit de app en probeer opnieuw in te loggen", function(){}, "Melding", "OK");
        //rest.createHeader(restcall,'128361283768');
      }
      else
      {
        var slaveName = device.uuid+'-'+dataset.item(0)['practiceID'];
        var post = {
          SlaveAccountName: slaveName,
          Password:null,
          DeviceType:device.model
        }
      /*
      $.ajax({
         url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
         type: 'POST',
         data: { restcall:"Users", extentie:"SlaveAccounts",postvars:post, instanceid:accountmanagement_users, type:"POST", token:token},
        dataType:'json',
           success: function(data) {
            account.saveSlave(data);
            statistics.register("createslave","200");

         },
         error: function(jqXHR, textStatus)
         {
            alert(jqXHR["status"]);
            navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");  
         }
         });
      */

        $.ajax(
        {
          url: 'https://api.pharmeon.nl/API/V1/Users/'+accountmanagement_users+'/SlaveAccounts',
          type: 'POST',
          headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
          dataType:'json',
          data: JSON.stringify(post),
          success: function(data)
          {
            account.saveSlave(data);
            statistics.register("createslave","200");
          },
          error: function(jqXHR, textStatus)
          {
            switch (jqXHR["status"])
            {
              case 400:
                navigator.notification.alert("Ontbrekende parameters voor het koppelen van het account aan de app", function(){}, "Helaas", "OK");
                statistics.register("createslave","400");
              break;
              case 401:
                navigator.notification.alert("Onvoldoende rechten om de app te koppelen aan het account", function(){}, "Helaas", "OK");
                statistics.register("createslave","401");
              break;
              case 500:
                navigator.notification.alert("Het is niet gelukt om de app te koppelen aan uw account. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
                statistics.register("createslave","500");
              break;
              default:
                navigator.notification.alert("Het is helaas niet gelukt om de app te koppelen aan uw account. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
                statistics.register("createslave","default");
            }
          }
        });
      }
    });
  });

},

slaveLogin:function(datum,autorisation)
{
  var post = {
    UserName: slavename,
    UserPassword: slavecode
  }
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"Sessions", extentie:"sessions",postvars:post, instanceid:accountmanagement_sessions, type:"POST", token:""},
    dataType:'json',
       success: function(data) {
        if(data['Token'])
        {
          account.storeToken(data['Token']);

          rest.getToken("getMedicationSettings");
          rest.getToken("getOpenQuestions");
          rest.getToken("showOpenAppointments");
          statistics.register("slavelogin","200");
         setup.checkForPincode();
      }
      $('#login-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        alert("error");
        alert(jqXHR["status"]);
        navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");  
         statistics.register("slavelogin",""+jqXHR["status"]+"");
     }
     });
*/

  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Sessions/'+accountmanagement_sessions+'/sessions',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
      if(data['Token'])
      {
        account.storeToken(data['Token']);

        rest.getToken("getMedicationSettings");
        rest.getToken("getOpenQuestions");
        rest.getToken("showOpenAppointments");
        //rest.getToken("linkStatus");
        statistics.register("slavelogin","200");
        setup.checkForPincode();
      }
    },
    error: function(jqXHR, textStatus)
    {
      //alert(jqXHR["status"]);
      navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      statistics.register("slavelogin",""+jqXHR["status"]+"");
    }
  });
},
 
login:function(datum,autorisation)
{
  var username = document.getElementById('username');
  var password = document.getElementById('password');
  
  var post = {
    UserName: username.value,
    UserPassword: password.value
  }
/*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"Sessions", extentie:"sessions",postvars:post, instanceid:accountmanagement_sessions, type:"POST", token:""},
    dataType:'json',
       success: function(data) {
        
      if(data['StatusCode'] == "OK")
      {
        account.storeToken(data['Token']);
        rest.getToken("getMedicationSettings");
        rest.getToken("getOpenQuestions");
        rest.getToken("showOpenAppointments");
        rest.getToken("createSlave");
        $.mobile.changePage("#firsttimepincode_page", { transition: "pop", changeHash: false }); 
        statistics.register("login","200");
      }
      if(data['StatusCode'] == "Unauthorized")
      {
        if(data['Token'] != null)
        {
            account.storeToken(data["Token"]);
            navigator.notification.alert("Er is een verificatiecode naar u verzonden. Vul deze in om de Medi App aan uw account te koppelen.", function(){}, "Verzonden", "OK");  
            $.mobile.changePage("#verifycode-page", { transition: "slide", changeHash: false });
            $('#login-loader').hide();
            statistics.register("login","session");
        }
        else
        {
          navigator.notification.alert("Het door u opgegeven gebruikersnaam en wachwoord is niet bekend. Probeer het nogmaals.", function(){}, "Helaas", "OK");  
            statistics.register("login","401");
        }
      }
      
      $('#login-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        alert(JSON.stringify(jqXHR));
        alert(jqXHR["status"]);
        navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");  
     }
     });
    */
    
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Sessions/'+accountmanagement_sessions+'/sessions',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data, textStatus, xhr)
    {
        if(xhr['status'] == '200')
        {
          account.storeToken(data['Token']);
          
          $.mobile.changePage("#firsttimepincode_page", { transition: "pop", changeHash: false }); 
          $('#login-loader').hide();
          rest.getToken("getMedicationSettings");
          rest.getToken("getOpenQuestions");
          rest.getToken("showOpenAppointments");
          rest.getToken("createSlave");
          statistics.register("login","200");
        }
        if(xhr['status'] == '202')
        {
          account.storeToken(data['Token']);
          navigator.notification.alert("Er is een verificatiecode naar u verzonden. Vul deze in om de Medi App aan uw account te koppelen.", function(){}, "Verzonden", "OK");  
          $.mobile.changePage("#verifycode-page", { transition: "slide", changeHash: false });
          $('#login-loader').hide();
          statistics.register("login","session");
        }
    },
    error: function(jqXHR, textStatus)
    {
      //alert(jqXHR["status"]);
      if(jqXHR["status"] == '401')
      {
          //alert(JSON.stringify(jqXHR["responseJSON"]));
          if(jqXHR["responseJSON"]["Token"] == null)
          {
            $('#login-loader').hide();
            navigator.notification.alert("Het door u opgegeven gebruikersnaam en wachwoord is niet bekend. Probeer het nogmaals.", function(){}, "Helaas", "OK");  
            statistics.register("login","401");
          }
          else
          {
            account.storeToken(jqXHR["responseJSON"]["Token"]);
            navigator.notification.alert("Er is een verificatiecode naar u verzonden. Vul deze in om de Medi App aan uw account te koppelen.", function(){}, "Verzonden", "OK");  
            $.mobile.changePage("#verifycode-page", { transition: "slide", changeHash: false });
            $('#login-loader').hide();
            statistics.register("login","session");
          }
      }
      else
      {
          $('#login-loader').hide();
          navigator.notification.alert("Het is niet gelukt om uw login gegevens te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");  
      }
    }
  });

},

securitycodeRequest:function(datum,autorisation,token)
{
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Sessions/'+accountmanagement_sessions+'/sessions/securitycode',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
       navigator.notification.alert("Er is een nieuwe verificatiecode naar u verzonden om de koppeling met de app af te ronden.", function(){}, "Verificatiecode verzonden", "OK");  
    },
    error: function(jqXHR, textStatus)
    {
        navigator.notification.alert("Het is niet gelukt om de beveiligingscode opnieuw te genereren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
    }
  });
},
 
verifySecurityCode:function(datum,autorisation,token)
{
  
  var code = document.getElementById('sessioncode').value;
  var post = {
    SecurityCode: code
  }
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"Sessions", extentie:"sessions/securityCode",postvars:post, instanceid:accountmanagement_sessions, type:"POST", token:token},
    dataType:'json',
       success: function(data) {
        switch (data)
        {
          case 401:
            navigator.notification.alert("U heeft een ongeldige verificatiecode ingevoerd", function(){}, "Helaas", "OK");
            $('#sessioncode-loader').hide();
            statistics.register("loginsession","401");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om de beveiligingscode te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            $('#sessioncode-loader').hide();
            statistics.register("loginsession","500");
          break;
          default:
            $.mobile.changePage("#firsttimepincode_page", { transition: "pop", changeHash: false }); 
      
            $('#sessioncode-loader').hide();
            //alert('hallo');
            rest.getToken("createSlave");
            statistics.register("loginsession","200");
        }

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de beveiligingscode te valideren. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        $('#sessioncode-loader').hide();
        statistics.register("loginsession","connectionerror");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Sessions/'+accountmanagement_sessions+'/sessions/securityCode',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
      //$.mobile.changePage("#dashboard-page", { transition: "fade", changeHash: false });
      $.mobile.changePage("#firsttimepincode_page", { transition: "pop", changeHash: false }); 
      
      $('#sessioncode-loader').hide();
      rest.getToken("createSlave");
      statistics.register("loginsession","200");
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("U heeft een ongeldige verificatiecode ingevoerd", function(){}, "Helaas", "OK");
          $('#sessioncode-loader').hide();
          statistics.register("loginsession","401");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de beveiligingscode te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          $('#sessioncode-loader').hide();
          statistics.register("loginsession","500");
        break;
        default:
          navigator.notification.alert("Het is helaas niet gelukt om de beveiligingscode te valideren.", function(){}, "Helaas", "OK");
          $('#sessioncode-loader').hide();
          statistics.register("loginsession","default");
      }
    }
  });
},

updateVerifySecurityCode:function(datum,autorisation,token)
{
  var code = document.getElementById('updatesessioncode').value;
  var post = {
    SecurityCode: code
  }
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"Sessions", extentie:"sessions/securityCode",postvars:post, instanceid:accountmanagement_sessions, type:"POST", token:token},
    dataType:'json',
       success: function(data) {
        switch (data)
        {
          case 401:
            navigator.notification.alert("U heeft een ongeldige verificatiecode ingevoerd", function(){}, "Helaas", "OK");
            $('#updatesessioncode-loader').hide();
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om de beveiligingscode te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            $('#updatesessioncode-loader').hide();
          break;
          default:
            rest.getToken("updateuser");
        }

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de beveiligingscode te valideren. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        $('#sessioncode-loader').hide();
        statistics.register("loginsession","connectionerror");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/Sessions/'+accountmanagement_sessions+'/sessions/securityCode',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
      $('#updatesessioncode-loader').hide();
      rest.getToken("updateuser");
      
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("U heeft een ongeldige verificatiecode ingevoerd", function(){}, "Helaas", "OK");
          $('#updatesessioncode-loader').hide();
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de beveiligingscode te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          $('#updatesessioncode-loader').hide();
        break;
        default:
          navigator.notification.alert("Het is helaas niet gelukt om de beveiligingscode te valideren.", function(){}, "Helaas", "OK");
          $('#updatesessioncode-loader').hide();
      }
    }
  });
},

getagendas:function(datum,autorisation,token)
{
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"agendas",postvars:"", instanceid:webagenda_agendas, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de beschikbare agenda's op te halen", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om beschikbare agenda's op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              agenda.selection(data);
              rest.getToken('getagendatext');
        }  
        $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om beschikbare agenda's op te halen. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        $('#dashboard-loader').hide();
     }
     });
  */
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_agendas+'/agendas',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
        agenda.selection(data);
        rest.getToken('getagendatext');
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
          case 401:
          navigator.notification.alert("Onvoldoende rechten om de beschikbare agenda's op te halen", function(){}, "Helaas", "OK");
          break;
          case 500:
          navigator.notification.alert("Het is niet gelukt om beschikbare agenda's op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          break;
          default:
              navigator.notification.alert("Het is niet gelukt om beschikbare agenda's op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
      $('#agenda-popup-loader').hide();
    }
  });
},

getagendatext:function(datum,autorisation,token)
{
  $('#agenda-selectiontext').empty();
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"settings",postvars:"", instanceid:webagenda_settings, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de agenda teksten op te halen", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de agenda teksten te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              $('#agenda-selectiontext').append(data['AgendaOverviewText']);
              $.mobile.changePage("#appointmentselectemployee_page", { transition: "slide", changeHash: false });
              $('#dashboard-loader').hide();
        }  
        $('#dashboard-loader').hide();
      },
      error: function(jqXHR, textStatus)
      {
        navigator.notification.alert("Het is niet gelukt om de agenda teksten te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        $('#dashboard-loader').hide();
      }
  });
*/
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_settings+'/settings',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      //alert(JSON.stringify(data));
      $('#agenda-selectiontext').append(data['AgendaOverviewText']);

      $.mobile.changePage("#appointmentselectemployee_page", { transition: "slide", changeHash: true });
      $('#agenda-popup-loader').hide();
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 401:
        navigator.notification.alert("Onvoldoende rechten om de agenda teksten op te halen", function(){}, "Helaas", "OK");
        break;
        case 500:
        navigator.notification.alert("Het is niet gelukt om de agenda teksten te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de agenda teksten te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
      $('#agenda-popup-loader').hide();
    }
  });
},

getavailabledates:function(datum,autorisation,token)
{
  var agendaID = document.getElementById('agendachoise');
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"agendas/"+agendaID.value+"",postvars:"", instanceid:webagenda_agendas, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de beschikbare dagen te laden.", function(){}, "Helaas", "OK");
            break;
            case 404:
              navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de beschikbare dagen voor de geselecteerde agenda op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              agenda.updatecalendar(data);
        }  


        $('#agenda-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de beschikbare dagen voor de geselecteerde agenda op te halen. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        $('#agenda-loader').hide();
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_agendas+'/agendas/'+agendaID.value,
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
        agenda.updatecalendar(data);
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Onvoldoende rechten om de beschikbare dagen te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
          navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de beschikbare dagen voor de geselecteerde agenda op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de beschikbare dagen voor de geselecteerde agenda op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
      $('#agenda-loader').hide();
    }
  });
},
    
getclosestdate:function(datum,autorisation,token)
{
  var agendaID = document.getElementById('agendachoise');
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"agendas/"+agendaID.value+"",postvars:"", instanceid:webagenda_agendas, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        //alert(JSON.stringify(data));
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de eerst beschikbare datum voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
            break;
            case 404:
              navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de eerst beschikbare datum voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              agenda.closestdate(data);
        }  
     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de eerst beschikbare datum voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
     }
  });
*/
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_agendas+'/agendas/'+agendaID.value,
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      //alert(JSON.stringify(data));
      agenda.closestdate(data);
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Onvoldoende rechten om de eerst beschikbare datum voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
          navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de eerst beschikbare datum voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de eerst beschikbare datum voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
    }
  });
},

getnearesttimeblock:function(datum,autorisation,token)
{
  $('#nearestdatetime').empty();
  var agendaID = document.getElementById('agendachoise');
  var agendaDate = document.getElementById('appointmentdate');
  var agendaTypeName = document.getElementById('agendaTypeName');
  var date = agendaDate.value;
  
  var res = date.split("T");
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"agendas/"+agendaID.value+"/"+agendaTypeName.value+"/timeslots/"+res[0]+"",postvars:"", instanceid:webagenda_agendas, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
          navigator.notification.alert("Onvoldoende rechten om het eerst beschikbare tijdstip voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
          navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om het eerst beschikbare tijdstip voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          agenda.nearestTime(data); 
        }  


        $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om het eerst beschikbare tijdstip voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_agendas+'/agendas/'+agendaID.value+'/'+agendaTypeName.value+'/timeslots/'+res[0],
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      agenda.nearestTime(data);
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Onvoldoende rechten om het eerst beschikbare tijdstip voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
          navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om het eerst beschikbare tijdstip voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om het eerst beschikbare tijdstip voor de geselecteerde agenda te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
    }
  });
},

countAvailableSlots:function(datum,autorisation,token)
{
  var agendaID = document.getElementById('agendachoise');
  var agendaDate = document.getElementById('calenderActiveDay');
  var agendaTypeName = document.getElementById('agendaTypeName');
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"agendas/"+agendaID.value+"/"+agendaTypeName.value+"/timeslots/"+agendaDate.value+"",postvars:"", instanceid:webagenda_agendas, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 400:
              navigator.notification.alert("Verkeerde parameters worden meeverzonden voor het berekenen van de beschikbare tijdstippen.", function(){}, "Helaas", "OK");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de beschikbare tijdstippen voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
            break;
            case 404:
              navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te berekenen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              $('#calctimeslots').empty();
              $('#calctimeslots').append('Er zijn nog '+data.length+' plekken beschikbaar');
        }  
     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te berekenen. Probeer het later nogmaals.", function(){}, "connectionerror", "OK");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_agendas+'/agendas/'+agendaID.value+'/'+agendaTypeName.value+'/timeslots/'+agendaDate.value,
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      $('#calctimeslots').empty();
      $('#calctimeslots').append('Er zijn nog '+data.length+' plekken beschikbaar');
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 400:
          navigator.notification.alert("Verkeerde parameters worden meeverzonden voor het berekenen van de beschikbare tijdstippen.", function(){}, "Helaas", "OK");
        break;
        case 401:
          navigator.notification.alert("Onvoldoende rechten om de beschikbare tijdstippen voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
          navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te berekenen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te berekenen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
    }
  });
}, 
    
getTimeSlots:function(datum,autorisation,token)
{
  var agendaID = document.getElementById('agendachoise');
  var agendaDate = document.getElementById('appointmentdate');
  var agendaTypeName = document.getElementById('agendaTypeName');
  var date = agendaDate.value;
  
  var res = date.split("T");
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php', 
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"agendas/"+agendaID.value+"/"+agendaTypeName.value+"/timeslots/"+res[0]+"",postvars:"", instanceid:webagenda_agendas, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 400:
              navigator.notification.alert("Verkeerde parameters worden meeverzonden voor het berekenen van de beschikbare tijdstippen.", function(){}, "Helaas", "OK");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de beschikbare tijdstippen voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
            break;
            case 404:
              navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            break;
            default:
              agenda.changetime(data);
        }  


        $('#dashboard-loader').hide();

     },
     error: function(jqXHR, textStatus)
     {
        navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_agendas+'/agendas/'+agendaID.value+'/'+agendaTypeName.value+'/timeslots/'+res[0],
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      agenda.changetime(data);
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 400:
          navigator.notification.alert("Verkeerde parameters worden meeverzonden voor het berekenen van de beschikbare tijdstippen.", function(){}, "Helaas", "OK");
        break;
        case 401:
          navigator.notification.alert("Onvoldoende rechten om de beschikbare tijdstippen voor de geselecteerde agenda te laden.", function(){}, "Helaas", "OK");
        break;
        case 404:
          navigator.notification.alert("Geselecteerde agenda kon niet worden gevonden.", function(){}, "Helaas", "OK");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de beschikbare tijdstippen te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
      }
      $('#agenda-loader').hide();
    }
  });
},
    
createappointment:function(datum,autorisation,token)
{
  var date = document.getElementById('appointmentdate');
  var time = document.getElementById('appointmenttime');
  var AgendaId = document.getElementById('agendachoise');
  var TimeSlotId = document.getElementById('appointmenttimeslotid');
  var timearray = time.value.split(" - ");
  var datearray = date.value.split("T");
  
  var StartDateTime = datearray[0]+'T'+timearray[0]+':00';
  
  var EndDateTime = datearray[0]+'T'+timearray[1]+':00';
  var Reason = document.getElementById('agendaToelichtingArea');
  
  var post = {
      AppointmentId:0,
      TimeSlotId:TimeSlotId.value,
      Description:Reason.value,
      CanBeModified:true,
      StartDateTime: StartDateTime,
      EndDateTime: EndDateTime,
      AgendaId:AgendaId.value,
      CreateReminder:false
      
  }

  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"WebAgenda", extentie:"appointments",postvars:post, instanceid:webagenda_appointments, type:"POST", token:token},
    dataType:'json',
       success: function(data) {
        switch (data)
        {
          case 400:
            navigator.notification.alert("Verkeerde parameters worden meeverzonden voor maken van een afspraak.", function(){}, "Helaas", "OK");
            statistics.register("createappointment","400");
          break;
          case 401:
            navigator.notification.alert("Onvoldoende rechten om afspraak in te boeken.", function(){}, "Helaas", "OK");
            statistics.register("createappointment","401");
          break;
          case 409:
            navigator.notification.alert("Er is al een afspraak ingeboekt voor dit tijdstip.", function(){}, "Helaas", "OK");
            statistics.register("createappointment","409");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om de afspraak in te boeken. Probeer het later nogmaals. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("createappointment","500");
          break;
          default:
            agenda.save(data);
            rest.getToken("getOpenQuestions");
            rest.getToken("showOpenAppointments");
            statistics.register("createappointment","200");
        }
        $('#afspraak-loader').hide();
      },
     error: function(jqXHR, textStatus)
     {
          navigator.notification.alert("Het is niet gelukt om de afspraak in te boeken. Probeer het later nogmaals. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
          statistics.register("createappointment","connectionerror");
          $('#afspraak-loader').hide();
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_appointments+'/appointments',
    type: 'POST',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
      agenda.save(data);
      rest.getToken("getOpenQuestions");
      rest.getToken("showOpenAppointments");
      statistics.register("createappointment","200");
    },
    error: function(jqXHR, textStatus)
    {
      switch (jqXHR["status"])
      {
        case 400:
          navigator.notification.alert("Verkeerde parameters worden meeverzonden voor maken van een afspraak.", function(){}, "Helaas", "OK");
          statistics.register("createappointment","400");
        break;
        case 401:
          navigator.notification.alert("Onvoldoende rechten om afspraak in te boeken.", function(){}, "Helaas", "OK");
          statistics.register("createappointment","401");
        break;
        case 409:
          navigator.notification.alert("Er is al een afspraak ingeboekt voor dit tijdstip.", function(){}, "Helaas", "OK");
          statistics.register("createappointment","409");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de afspraak in te boeken. Probeer het later nogmaals. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("createappointment","500");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de afspraak in te boeken. Probeer het later nogmaals. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("createappointment","default");
      }
      $('#afspraak-loader').hide();
    }
  });
},

getappointments:function(datum,autorisation,token)
{
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"appointments",postvars:"", instanceid:webagenda_appointments, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de afspraken te laden.", function(){}, "Helaas", "OK");
              statistics.register("getappointments","401");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om een overzicht van uw afspraken op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("getappointments","500");
            break;
            default:
              agenda.overview(data);
              statistics.register("getappointments","200");
        }  
      },
      error: function(jqXHR, textStatus)
      {
        navigator.notification.alert("Het is niet gelukt om een overzicht van uw afspraken op te halen. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
        statistics.register("getappointments","connectionerror");
        $('#dashboard-loader').hide();
      }
});
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_appointments+'/appointments',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      agenda.overview(data);
      statistics.register("getappointments","200");
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Onvoldoende rechten om de afspraken te laden.", function(){}, "Helaas", "OK");
          statistics.register("getappointments","401");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om een overzicht van uw afspraken op te halen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("getappointments","500");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om een overzicht van uw afspraken op te halen. Probeer het later nogmaals. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("getappointments","default");
      }
      $('#agenda-popup-loader').hide();
    }
  });
},

showOpenAppointments:function(datum,autorisation,token)
{
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"WebAgenda", extentie:"appointments",postvars:"", instanceid:webagenda_appointments, type:"GET", token:token},
    dataType:'json',
       success: function(data) {
        //alert(JSON.stringify(data)); 
        dashboard.appointments(data);
        

     },
     error: function(jqXHR, textStatus)
     {
      
      //navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om uw medicatie overzicht te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
     }
     });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_appointments+'/appointments',
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
      dashboard.appointments(data);
    },
    error: function()
    {
      
    }
  });
},

getappointmentdetails:function(datum,autorisation,token)
{
  var appointmentID = document.getElementById('details_appointmentid');
  /*
  $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { restcall:"WebAgenda", extentie:"appointments/"+appointmentID.value+"",postvars:"", instanceid:webagenda_appointments, type:"GET", token:token},
      dataType:'json',
      success: function(data) {
        switch (data)
        {
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de afspraakdetails te laden.", function(){}, "Helaas", "OK");
              statistics.register("appointmentdetails","401");
            break;
            case 404:
              navigator.notification.alert("De geselecteerde afspraak kan niet worden gevonden / geladen.", function(){}, "Helaas", "OK");
              statistics.register("appointmentdetails","404");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de afspraakdetails te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("appointmentdetails","500");
            break;
            default:
              agenda.showdetails(data);
              statistics.register("appointmentdetails","200");
        }  
      },
      error: function(jqXHR, textStatus)
      {
         navigator.notification.alert("Het is niet gelukt om de afspraakdetails te laden. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
              statistics.register("appointmentdetails","connectionerror");
      }
  });
  */
  
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_appointments+'/appointments/'+appointmentID.value,
    type: 'GET',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    success: function(data)
    {
        agenda.showdetails(data);
        statistics.register("appointmentdetails","200");
    },
    error: function()
    {
      switch (jqXHR["status"])
      {
        case 401:
          navigator.notification.alert("Onvoldoende rechten om de afspraakdetails te laden.", function(){}, "Helaas", "OK");
          statistics.register("appointmentdetails","401");
        break;
        case 404:
          navigator.notification.alert("De geselecteerde afspraak kan niet worden gevonden / geladen.", function(){}, "Helaas", "OK");
          statistics.register("appointmentdetails","404");
        break;
        case 500:
          navigator.notification.alert("Het is niet gelukt om de afspraakdetails te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("appointmentdetails","500");
        break;
        default:
          navigator.notification.alert("Het is niet gelukt om de afspraakdetails te laden. Probeer het later nogmaals. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
          statistics.register("appointmentdetails","default");
      }
    }
  });
},

removeAppointment: function(datum,autorisation,token)
{
  $("#appointment-detail-loader").show();
  var appointmentID = document.getElementById('details_appointmentid');
  var timesloteid = document.getElementById('details_timeslotid');
  var description = document.getElementById('details_description');
  var startdatetime = document.getElementById('details_startdatetime');
  var enddatetime = document.getElementById('details_enddatetime');
  var agendaid = document.getElementById('details_agendaid');
  
  var post = {
    AppointmentId:appointmentID.value,
    TimeSlotId:timesloteid.value,
    Description:description.value,
    CanBeModified:true,
    StartDateTime: startdatetime.value,
    EndDateTime: enddatetime.value,
    AgendaId:agendaid.value,
    CreateReminder:false
  }
  /*
  $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { restcall:"WebAgenda", extentie:"appointments/"+appointmentID.value+"",postvars:post, instanceid:webagenda_appointments, type:"DELETE", token:token},
    dataType:'json',
       success: function(data) {
        switch (data)
        {
          case 400:
            navigator.notification.alert("Afspraak kon niet worden verwijderd er ontbreken gegevens.", function(){}, "Helaas", "OK");
            statistics.register("removeappointment","401");
          break;
          case 401:
            navigator.notification.alert("Onvoldoende rechten om de afspraak te verwijderen.", function(){}, "Helaas", "OK");
            statistics.register("removeappointment","401");
          break;
          case 404:
            navigator.notification.alert("De geselecteerde afspraak kan niet worden gevonden.", function(){}, "Helaas", "OK");
            statistics.register("removeappointment","404");
          break;
          case 500:
            navigator.notification.alert("Het is niet gelukt om de afspraak te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
            statistics.register("removeappointment","500");
          break;
          default:
            navigator.notification.alert("De afspraak is verwijderd uit de agenda.", function(){}, "Annuleren", "OK");
        
            rest.getToken("getOpenQuestions");
            rest.getToken("showOpenAppointments");
            $("#appointment-detail-loader").hide();
            $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false }); 
            statistics.register("removeappointment","200");
        }
     },
     error: function(jqXHR, textStatus)
     {
        switch (jqXHR["status"])
        {
            case 200:
              navigator.notification.alert("De afspraak is verwijderd uit de agenda.", function(){}, "Annuleren", "OK");
          
              rest.getToken("getOpenQuestions");
              rest.getToken("showOpenAppointments");
              $("#appointment-detail-loader").hide();
              $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false }); 
              statistics.register("removeappointment","200");
            break;
            case 400:
            navigator.notification.alert("Afspraak kon niet worden verwijderd er ontbreken gegevens.", function(){}, "Helaas", "OK");
            statistics.register("removeappointment","401");
            break;
            case 401:
              navigator.notification.alert("Onvoldoende rechten om de afspraak te verwijderen.", function(){}, "Helaas", "OK");
              statistics.register("removeappointment","401");
            break;
            case 404:
              navigator.notification.alert("De geselecteerde afspraak kan niet worden gevonden.", function(){}, "Helaas", "OK");
              statistics.register("removeappointment","404");
            break;
            case 500:
              navigator.notification.alert("Het is niet gelukt om de afspraak te verwijderen. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
              statistics.register("removeappointment","500");
            break;
            default:
              navigator.notification.alert("Er is een onbekende fout opgetreden. Het is niet gelukt om de afspraak te verwijderen. Probeer het later nogmaals.", function(){}, "Verbindingsfout", "OK");
              statistics.register("removeappointment","unknown");
        }
     }
     });
*/
    
  $.ajax(
  {
    url: 'https://api.pharmeon.nl/API/V1/WebAgenda/'+webagenda_appointments+'/appointments/'+appointmentID.value,
    type: 'DELETE',
    headers: {"Pharmeon-RestApi-Date": datum, "Authorization":autorisation, "Token":token},
    dataType:'json',
    data: JSON.stringify(post),
    success: function(data)
    {
      navigator.notification.alert("De afspraak is verwijderd uit de agenda.", function(){}, "Annuleren", "OK");
      
      rest.getToken("getOpenQuestions");
      rest.getToken("showOpenAppointments");
      $("#appointment-detail-loader").hide();
      $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false }); 
      statistics.register("removeappointment","200");

    },
    error: function(jqXHR, textStatus)
    {
      if(jqXHR["status"] == '200')
      {
        navigator.notification.alert("De afspraak is geannuleerd en verwijderd uit de agenda.", function(){}, "Annuleren", "OK");

        rest.getToken("getOpenQuestions");
        rest.getToken("showOpenAppointments");
        $("#appointment-detail-loader").hide();
        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false }); 
        statistics.register("removeappointment","200");
      }
      else
      {
        navigator.notification.alert("Het is niet gelukt om de afspraak te annuleren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
        $("#appointment-detail-loader").hide();
        statistics.register("removeappointment",""+jqXHR["status"]+"");
      }
    }
  });
},
    
getToken:function(restcall)
{
  $('#medicatiorequest_overview').empty();
  var query = "SELECT * FROM tblToken";
  
  db.transaction(function(tx)
  {
    tx.executeSql(query,[],function(tx, result)
    {
      dataset = result.rows;
      if(dataset.length == 0)
      {
        navigator.notification.alert("Er is geen geldige sessiecode gevonden. Sluit de app en probeer opnieuw in te loggen", function(){}, "Melding", "OK");
        //rest.createHeader(restcall,'128361283768');
      }
      else
      {
        rest.createHeader(restcall,dataset.item(0)['token']);
      }
    });
  });
}

};

var account = {

saveSlave:function(data)
{
  
  var insertToken = "INSERT INTO tblSlaveAccount (name,code) VALUES (?,?)";
        
  db.transaction(function(tx, results)
  {
      tx.executeSql(insertToken,[''+data["SlaveAccountName"]+'',''+data["Password"]+''],dbase.succesCB,dbase.errorCB);
  });
},

storeToken:function(token)
    {
        var deleteToken = "DELETE FROM tblToken";
        var insertToken = "INSERT INTO tblToken (token) VALUES (?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(deleteToken,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(insertToken,[''+token+''],dbase.succesCB,dbase.errorCB);
        });
    },

savePincode:function(type)
{
  var pw1 = document.getElementById(''+type+'pw1');
  var pw2 = document.getElementById(''+type+'pw2');
  var pw3 = document.getElementById(''+type+'pw3');
  var pw4 = document.getElementById(''+type+'pw4');
  var pw5 = document.getElementById(''+type+'pw5');
  
  if(pw1.value == "" || pw2.value == "" || pw3.value == "" || pw4.value == "" || pw5.value == "")
  {
      navigator.notification.alert("U dient een 5 cijferige pincode in te stellen", function(){}, "Melding", "OK");
  }
  else
  {
      var password = ''+pw1.value+''+pw2.value+''+pw3.value+''+pw4.value+''+pw5.value;
      var deletequery = "DELETE FROM tblPincode";
      var insertPincode = "INSERT INTO tblPincode (pincode) VALUES (?)";
      
      db.transaction(function(tx, results)
      {
          tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
          tx.executeSql(insertPincode,[password],dbase.succesCB,dbase.errorCB);
                     
          navigator.notification.alert("Uw pincode voor de app is ingesteld.", function(){}, "Melding", "OK");
          $("#setPinform")[0].reset();
          if(type == 'first')
          {
            rest.getToken("linkStatus");
            $("#setPinform")[0].reset();
            //$.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });  
          }
          else
          {
            $.mobile.changePage("#settings-page", { transition: "pop", changeHash: false });  
            $("#resetPinform")[0].reset();
          }
      });
  }

},
  
verifyAccountFields:function(type)
    {
        var gender = document.getElementById(type+"_gender").value;
        var initials = document.getElementById(type+"_initials").value;
        var firstname = document.getElementById(type+"_firstname").value;
        var lastname = document.getElementById(type+"_lastname").value;
        var birthdate = document.getElementById(type+"_birthdate").value;
        var street = document.getElementById(type+"_street").value;
        var housenumber = document.getElementById(type+"_housenumber").value;
        var postalcode = document.getElementById(type+"_postalcode").value;
        var city = document.getElementById(type+"_city").value;
        var mobilenumber = document.getElementById(type+"_mobilenumber").value;
        var email = document.getElementById(type+"_email").value;
        
        if(type == "register")
        {
            var password = document.getElementById(type+"_password").value;
            var password2 = document.getElementById(type+"_password2").value;
        }
        
        if (initials=="" || firstname=="" || lastname=="" || birthdate=="" || street=="" || housenumber=="" || postalcode=="" || city=="" || mobilenumber=="" || email=="" || password=="" || password=="" )
        {
            navigator.notification.alert("U dient alle verplichte velden in te vullen om uw registratie te voltooien", function(){}, "Melding", "OK");
            return;
        }
        
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(!email.match(mailformat))
        {
            navigator.notification.alert("U dient een geldig e-mailadres in te vullen om uw registratie te voltooien.", function(){}, "Melding", "OK");
            return;
        }
        
        if(password !== password2)
        {
            navigator.notification.alert("U dient twee keer hetzelfde wachtwoord in te voeren om uw registratie te voltooien.", function(){}, "Melding", "OK");
            return;

        }
        if(type == 'update')
        {
            $("#usersettings-loader").show();
            //rest.getToken('updateuser');
            account.updateQuestion();
        }
        else
        {
            rest.createHeader('createuser','');
        }
    },

updateQuestion:function()
{
  navigator.notification.confirm('Om uw gegevens bij te werken dient u nogmaals uw gebruikersnaam en wachtwoord in te voeren. Wilt u doorgaan?',
  function(button)
  {
    if (button == 1)
    {
      //yes
      //rest.createHeader('updateUserLogin');
      $.mobile.changePage("#updatelogin-page", { transition: "pop", changeHash: false }); 
    }
  },
  'Bijwerken',
  ['Ja','Nee']
  );
}, 

showuserinfo:function(data)
  {
    var birthdate = data['Birthdate'].split("T");
    
    $('#AccountId').val(data['AccountId']);
    $('#UserName').val(data['UserName']);
    $('#update_email').val(data['Email']);
    $('#update_gender').val(data['Gender']).change();
    $('#update_mobilenumber').val(data['MobileNumber']);
    $('#update_birthdate').val(birthdate[0]);
    $('#update_firstname').val(data['FirstName']);
    $('#update_lastname').val(data['LastName']);
    $('#update_infix').val(data['Infix']);
    $('#update_initials').val(data['Initials']);
    
    $('#update_street').val(data['AddressList'][0]['Street']);
    $('#update_housenumber').val(data['AddressList'][0]['HouseNumber']);
    $('#update_city').val(data['AddressList'][0]['City']);
    $('#update_postalcode').val(data['AddressList'][0]['PostalCode']);
  },

  logout:function()
  {
    navigator.notification.confirm('Weet u zeker dat u wilt uitloggen? U dient de volgende keer opnieuw uw huisarts en inloggegevens in te voeren.',
    function(button)
    {
      if (button == 1)
      {
        //yes
        var emptyToken = "DELETE FROM tblToken";
        var emptyPractice = "DELETE FROM tblPractice";
        var emptyMedication = "DELETE FROM tblMedication";
        var emptyRequestList = "DELETE FROM tblRequestList";
        var emptyPincode = "DELETE FROM tblPincode";
        var emptySlaveAccount = "DELETE FROM tblSlaveAccount";
        db.transaction(function(tx, results)
        {
            tx.executeSql(emptyToken,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(emptyPractice,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(emptyMedication,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(emptyRequestList,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(emptyPincode,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(emptySlaveAccount,[],dbase.succesCB,dbase.errorCB);
            setup.init();
        });
      }
    },
    'Uitloggen',
    ['Ja','Nee']
    );
  },
};

var setup = {
bodyLoad:function()
    {
        document.addEventListener("deviceready", setup.deviceReady, false);
    },
    
deviceReady:function()
    {
        document.addEventListener("resume", setup.checkForPractice, false);
        setup.init();
    },

init:function()
    {
        dbase.openDB();
    },
  
checkForPractice:function()
{
  var query = "SELECT * FROM tblPractice";
        
    db.transaction(function(tx)
    {
        tx.executeSql(query,[],function(tx, result)
        {
            
            dataset = result.rows;
            if(dataset.length == 0)
            {
              practice.selection();
            }
            else
            {
              practice.setInstances();
              setup.checkForSlave();
            }

        });
    });
},

  checkForSlave:function()
  {
    var query = "SELECT * FROM tblSlaveAccount";
        
    db.transaction(function(tx)
    {
        tx.executeSql(query,[],function(tx, result)
        {
            dataset = result.rows;
            if(dataset.length == 0)
            {
              $.mobile.changePage("#login-page", { transition: "pop", changeHash: false });   
            }
            else
            {
              slavename = dataset.item(0)["name"];
              slavecode = dataset.item(0)["code"];

              rest.createHeader('slaveLogin','');
            }
        });
    });
  },

  checkForPincode:function()
  {
    var query = "SELECT * FROM tblPincode";
        
    db.transaction(function(tx)
    {
        tx.executeSql(query,[],function(tx, result)
        {
          dataset = result.rows;
          if(dataset.length == 0)
          {
            $.mobile.changePage("#firsttimepincode_page", { transition: "pop", changeHash: false });   

          }
          else
          {
            $.mobile.changePage("#validatepincode_page", { transition: "pop", changeHash: false });   
          }
        });
    });
  },

  validatePincode:function(type)
  {
    var pw1 = document.getElementById(''+type+'pw1');
    var pw2 = document.getElementById(''+type+'pw2');
    var pw3 = document.getElementById(''+type+'pw3');
    var pw4 = document.getElementById(''+type+'pw4');
    var pw5 = document.getElementById(''+type+'pw5');
    
    if(pw1.value == "" || pw2.value == "" || pw3.value == "" || pw4.value == "" || pw5.value == "")
    {
        navigator.notification.alert("U dient een 5 cijferige pincode in te voeren", function(){}, "Melding", "OK");
        $("#validatePinform")[0].reset();
    }
    else
    {
      var password = ''+pw1.value+''+pw2.value+''+pw3.value+''+pw4.value+''+pw5.value;
      var query = "SELECT * FROM tblPincode WHERE pincode = '"+password+"'";
      
      db.transaction(function(tx)
      {
          tx.executeSql(query,[],function(tx, result)
          {
            dataset = result.rows;
            if(dataset.length == 0)
            {
              navigator.notification.alert("U heeft een ongeldige code ingevoerd, probeer het nogmaals", function(){}, "Melding", "OK");  
              $("#validatePinform")[0].reset();
            }
            else
            {
              //$.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
              //rest.getToken("LinkStatus");
              rest.getToken("linkStatus");
              $("#validatePinform")[0].reset();
            }
        });
      });
    }
  },
};

var practice = {
  
  selection: function()
  {
    $.mobile.changePage("#selectpractice-page", { transition: "pop", changeHash: false });   
    $('#gpList').empty();
    $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { getList:'getlist'},
     success: function(data) {
      $('ul#gpList').append(data).listview('refresh');
     },
     error: function()
     {
      navigator.notification.alert("Er is een onverwachte fout opgetreden bij het ophalen van de beschikbare huisartsen.", function(){}, "Helaas", "OK");
     }
     });
  },

  confirm: function(customerId, practiceName)
  {
      navigator.notification.confirm('Wilt u de app koppelen met '+practiceName+ '?',
      function(button)
      {
        if (button == 1)
        {
          //yes
          practice.getSettings(customerId, practiceName);
         
        }
      },
      'Koppelen',
      ['Ja','Nee']
    );
  },

  getSettings: function(customerId, practiceName)
  {
    $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      dataType:'json',
      data: { getInstances:customerId},
      success: function(data) {
        practice.saveSettings(data);
      },
      error: function()
      {
        navigator.notification.alert("Er is een onverwachte fout opgetreden bij het configuren van de app.", function(){}, "Helaas", "OK");
      }
    });
  },

  saveSettings: function(data)
  {
      var uniqueidentifier = Math.floor(100000 + Math.random() * 900000);
      var insertSettings = "INSERT INTO tblPractice (practiceID, customerId, accountmanagement_users, accountmanagement_sessions, webagenda_settings, webagenda_appointments, webagenda_agendas, econsult, hhr) VALUES (?,?,?,?,?,?,?,?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertSettings,[''+uniqueidentifier+'', data["customerId"], data["accountmanagement_users"], data["accountmanagement_sessions"], data["webagenda_settings"], data["webagenda_appointments"], data["webagenda_agendas"], data["econsult"], data["hhr"]],dbase.succesCB,dbase.errorCB);
            practice.setInstances();
            $.mobile.changePage("#login-page", { transition: "pop", changeHash: false });           
        });
  },

  setInstances: function()
  {
    var query = "SELECT * FROM tblPractice";
        
    db.transaction(function(tx)
    {
        tx.executeSql(query,[],function(tx, result)
        {
          dataset = result.rows;
          if(dataset.length == 0)
          {
              navigator.notification.alert("Er zijn geen praktijkgegevens gevonden. Sluit de app en probeer opnieuw in te loggen", function(){}, "Melding", "OK");
          }
          else
          {
            //setting the global variables for use of the REST calls
            selectedCustomerId = dataset.item(0)['customerId'];
            accountmanagement_users = dataset.item(0)['accountmanagement_users'];
            accountmanagement_sessions = dataset.item(0)['accountmanagement_sessions'];
            webagenda_settings = dataset.item(0)['webagenda_settings'];
            webagenda_appointments = dataset.item(0)['webagenda_appointments'];
            webagenda_agendas = dataset.item(0)['webagenda_agendas'];
            econsult = dataset.item(0)['econsult'];
            hhr = dataset.item(0)['hhr'];
          }
        });
    });
  },

  getinfo: function()
  {
    
    $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { practiceinfo:selectedCustomerId},
      success: function(data) {
        $('#practiceInfo').empty();
        $('#practiceInfo').append(data);
        $('#practice-page').trigger('pagecreate');
        $.mobile.changePage("#practice-page", { transition: "slide", changeHash: true }); 
      },
      error: function()
      {
        navigator.notification.alert("Er is een onverwachte fout opgetreden bij het configuren van de app.", function(){}, "Helaas", "OK");
      }
    });
    
    
  },

  disclaimer:function()
  {
    $('#disclaimerInfo').empty();
    $.ajax({
      url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
      type: 'POST',
      data: { disclaimer:selectedCustomerId},
      success: function(data) {
        $('#disclaimerInfo').append(data);
        $('#disclaimerInfo').trigger('create');
      },
      error: function()
      {
        navigator.notification.alert("Er is een onverwachte fout opgetreden bij het configuren van de app.", function(){}, "Helaas", "OK");
      }
    });
    
    $.mobile.changePage("#disclaimer-page", { transition: "slide", changeHash: true }); 
  }
};

//capsulating for the database calls

// Create the Database
var db = window.openDatabase("uwzorgapp", "1.0", "Uw Zorg App", 200000);

var dbase = {

openDB:function()
    {
        db.transaction(dbase.createDB, dbase.errorCB, dbase.succesCB);
    },
    
createDB:function(tx)
    {
        //tx.executeSql('DROP TABLE IF EXISTS tblToken'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblToken (token TEXT)'); 

        //tx.executeSql('DROP TABLE IF EXISTS tblSlaveAccount'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblSlaveAccount (name TEXT, code TEXT)');         
        
        //tx.executeSql('DROP TABLE IF EXISTS tblPractice'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblPractice (practiceID TEXT, customerId TEXT, accountmanagement_users TEXT, accountmanagement_sessions TEXT, webagenda_settings TEXT, webagenda_appointments TEXT, webagenda_agendas TEXT, econsult TEXT, hhr TEXT)');
         
        tx.executeSql('DROP TABLE IF EXISTS tblMedication'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblMedication (MedicationId TEXT, MedicationName TEXT, AmountPerRepetition INTEGER, Unity TEXT, UsageText TEXT, PrescriberName TEXT, StartDate TEXT, EndDate TEXT, AllowToOrder TEXT, Prk TEXT, Hpk TEXT, IsHistory TEXT)');
        
        tx.executeSql('DROP TABLE IF EXISTS tblRequestList'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblRequestList (listID TEXT, medicationName, orderAmount TEXT)');

        //tx.executeSql('DROP TABLE IF EXISTS tblPincode'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblPincode (pincode TEXT, enterdate TEXT)');
        /*
        tx.executeSql('DROP TABLE IF EXISTS tblQuitedList'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblQuitedList (quitID TEXT)');
        
        tx.executeSql('DROP TABLE IF EXISTS tblChangedList'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblChangedList (changeID TEXT, newPrescription TEXT)');
        
        tx.executeSql('DROP TABLE IF EXISTS tblConsulten'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblConsulten (consultID TEXT, date TEXT, professional TEXT, subject TEXT, description TEXT, answer TEXT, status TEXT)');
        
        tx.executeSql('DROP TABLE IF EXISTS tblAppointments'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblAppointments (appointmentID TEXT, date TEXT, time TEXT, day TEXT, professional TEXT, reason TEXT, status TEXT)');
        */
        setup.checkForPractice();
        //dbase.insertData();
    },

//insert dummy data for demo purposes
    
insertData: function()
    {
        var insertMedication = "INSERT INTO tblMedication (medicationID, medicationName, amount, prescription, startdate, enddate, status) VALUES (?,?,?,?,?,?,?)";
        //tx.executeSql('CREATE TABLE IF NOT EXISTS tblMedication (MedicationId TEXT, MedicationName TEXT, amountPerRepetition TEXT, Unity TEXT, UsageText TEXT, PrescriberName TEXT, StartDate TEXT, EndDate TEXT, AllowToOrder TEXT, Prk TEXT, Hpk TEXT, IsHistory TEXT)');
        var oneWeekAgo = new Date();
        oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
        
        var reaction = "Op basis van uw klachten adviseer ik u om op korte termijn een afspraak in te plannen om uw klachten uitgebreider te bespreken. U kunt hiervoor gebruik maken van de afspraak mogelijkheid in de app.";
        
        var insertConsults = "INSERT INTO tblConsulten (consultID, date, professional, subject, description, answer, status) VALUES (?,?,?,?,?,?,?)";
        
        var insertAppointment = "INSERT INTO tblAppointments (appointmentID, date, time, day, professional, reason, status) VALUES (?,?,?,?,?,?,?)";
        
        db.transaction(function(tx, results)
        {
            //tx.executeSql(insertMedication,['1', 'URSOCHOL TABLET 300MG', '15 stuks', 'Gebruik volgens aanwijzingen bijsluiter', '17-09-2015', '17-04-2016','order'],dbase.succesCB,dbase.errorCB);
            
            //tx.executeSql(insertMedication,['2', 'IBUPROFEN CAPSULE 200MG', '50 stuks', '1 maal per dag, 1 capsule', '08-10-2014', '23-06-2016','order'],dbase.succesCB,dbase.errorCB);
                       
            tx.executeSql(insertConsults,['666', ''+oneWeekAgo+'', 'Fleur Steen', 'Regelmatig last van hoofdpijn', 'Meerdere malen per week heb ik enorm veel last van hoofdpijn. Zowel doordeweeks na een drukke werkdag, maar ook in het weekend', ''+reaction+'','closed'],dbase.succesCB,dbase.errorCB);
                       
            tx.executeSql(insertAppointment,['777', '17-2-2016', '08:30 - 08:35', 'Woensdag', 'Fleur Steen', 'Controle afspraak voor mijn rugklachten','closed'],dbase.succesCB,dbase.errorCB);
                       
        });
    },
    
errorCB:function(tx, err)
    {
        console.log("error processing SQL: "+err.code);
        //alert("error message::"+err.message);
    },
    
succesCB:function()
    {
        //alert("Query is correct uitgevoerd");
    },
succesCB2:function()
    {
        alert("Query is correct uitgevoerd");
    },
    
};


//capsulating for the medication overview
var medication = {

overview: function(data)
    {
      $('#medicationoverview').empty();
      
      for (var i = 0, item =null; i < data.length; i++)
      {
          item = data[i];
          
          var start = new Date(item['StartDate']);   
          var startday = start.getDate();
          var startmonth = start.getMonth();
          var startyear = start.getFullYear();
          
          var end = new Date(item['EndDate']);
          var endday = end.getDate();
          var endmonth = end.getMonth();
          var endyear = end.getFullYear();

          var startdatum = startday+' '+monthList[startmonth]+' '+startyear;
          var enddatum = endday+' '+monthList[endmonth]+' '+endyear;
          
          var prescriber = '';
          var hpk = item['Hpk'];
          var prk = item['Prk'];
          
          //alert(JSON.stringify(item["Reminder"]));
          var reminderButton = '<div class="reminder" onclick="medication.reminderpage(\''+prk+'\', \''+hpk+'\', \''+item["MedicationName"]+'\')"><i class="fa fa-bell" aria-hidden="true"></i> Bestelherinnering instellen</div></div>';
          var reminderNotification = '';
          if (item["Reminder"] !== null) {
            
            var reminderButton = '<div class="reminder red" onclick="medication.deleteReminder(\''+prk+'\', \''+hpk+'\', \''+item["MedicationName"]+'\')"><i class="fa fa-bell" aria-hidden="true"></i> Herinnering verwijderen</div></div>';
            
            var remindDate = new Date(item["Reminder"]["Date"]);   
            var remindDateday = remindDate.getDate();
            var remindDatemonth = remindDate.getMonth();
            var remindDateyear = remindDate.getFullYear();
            var reminderdatum = remindDateday+' '+monthList[remindDatemonth]+' '+remindDateyear;
            if(item["Reminder"]["Interval"] != null)
            {
              var reminderNotification = '<strong>Herinnering op:</strong> '+reminderdatum+'<br /><strong>Keert terug:</strong> '+reminderFrequenties[item["Reminder"]["Interval"]];
            }
            else
            {
              var reminderNotification = '<strong>Herinnering op:</strong> '+reminderdatum+'';  
            }
          }
          
          if(item['PrescriberName'] !== null)
          {
            var prescriber = '<strong>Voorgeschreven door: '+item['PrescriberName']+'</strong><br />';
          }
          
          $('#medicationoverview').append('<section class="medicationList" id="med_\''+item['MedicationId']+'\'" onclick="medication.details(\''+item['MedicationId']+'\')"><div class="medicationContainer"><div class="title">'+item['MedicationName']+'</div><div class="message">'+item["UsageText"]+'</div></div><div class="chevron"><i id="icon_'+item['MedicationId']+'" class="fa fa-chevron-right"></i></div><div class="clearer"></div><div class="medicationDetails" id="meddetail_'+item['MedicationId']+'"><div class="message"><strong>Receptdatum</strong>: '+startdatum+'<br /><strong>Voldoende tot</strong>: '+enddatum+'<br />'+prescriber+'<br />'+reminderNotification+'</div>'+reminderButton+'</section>');
      }

      $.mobile.changePage("#medication-page", { transition: "slide", changeHash: true });
    },

    details: function(id)
    {
        $( "#meddetail_"+id+"").slideToggle( "slow" );
        $("#icon_"+id+"").toggleClass("fa-chevron-right fa-chevron-down");
    },

reminderpage:function(prk, hpk, medicationName)
{
  $("#repeattype").selectmenu();
  $('#reminder_hpk').val(''+hpk+'');
  $('#reminder_prk').val(''+prk+'');
  $('#reminder_medicationName').val(''+medicationName+'');
  $('#reminderDate').val('');
  $('#repeatInterval').val('');
  //$('#repeattype').val('');
  $.mobile.changePage("#setreminder-page", { transition: "slideup", changeHash: true });
  $("#repeattype").selectmenu("refresh",true);
  
   
  
},

toggleFrequenty:function()
{
  var classname = document.getElementById('repeattype').value;
  if(classname == "1")
  {
    $("#reminderFrequency").removeClass("hidefrequenty");
    $("#reminderFrequency").addClass("showfrequenty");
  }
  else
  {
    $("#reminderFrequency").removeClass("showfrequenty");
    $("#reminderFrequency").addClass("hidefrequenty");
  }
  
},

deleteReminder:function(prk, hpk, medicationName)
{
  navigator.notification.confirm('Wilt u de herinnering voor '+medicationName+' verwijderen?',
      function(button) 
      {
        if (button == 1)
        {
          //yes
          deleteprk = ''+prk+'';
          deletehpk = ''+hpk+'';
          deleteName = medicationName;
          rest.getToken('deleteReminder');
        }
      },
      'Verwijderen',
      ['Ja','Nee']
    );

}

/*
undoQuit: function(id)
    {
        var undoQuit = "DELETE FROM tblQuitedList WHERE quitID = "+id;
        db.transaction(function(tx, results)
        {
            tx.executeSql(undoQuit,[],dbase.succesCB,dbase.errorCB);
        });
        
        medication.overview();
    },
    
quit: function(id)
    {
        $('#removeMedicationid').val(''+id+'');
        $.mobile.changePage("#quitmedication-page", { transition: "slideup", changeHash: false });
        medication.details(id);
    },

saveQuit: function()
    {
        var medicationID = ''+removeMedicationid.value+'';
        
        var insertMedication = "INSERT INTO tblQuitedList (quitID) VALUES (?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertMedication,[''+medicationID+''],dbase.succesCB,dbase.errorCB);
        });
        
        $.mobile.changePage("#medication-page", { transition: "slidedown", changeHash: false });
        medication.overview();
        
    },

changePrescription: function(id)
    {
        $('#changeMedicationid').val(''+id+'');
        $.mobile.changePage("#changemedication-page", { transition: "slideup", changeHash: false });
        medication.details(id);
    },
    
saveChange: function()
    {
        var medicationID = ''+changeMedicationid.value+'';
        var newPrescription = ''+changeToelichtingArea.value+'';
        
        var insertMedication = "INSERT INTO tblChangedList (changeID, newPrescription) VALUES (?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertMedication,[''+medicationID+'',''+newPrescription+''],dbase.succesCB,dbase.errorCB);
        });
        
        $.mobile.changePage("#medication-page", { transition: "slidedown", changeHash: false });
        medication.overview();
    },
    
removeChange: function(id)
    {
        var undoChange = "DELETE FROM tblChangedList WHERE changeID = "+id;
        db.transaction(function(tx, results)
        {
            tx.executeSql(undoChange,[],dbase.succesCB,dbase.errorCB);
        });
        
        medication.overview();
    },
    */
};

//capsulating for ordering medication
var request = {

clearRequestList: function()
{
  var emptyRequestList = "DELETE FROM tblRequestList";
  db.transaction(function(tx, results)
  {
    tx.executeSql(emptyRequestList,[],dbase.succesCB,dbase.errorCB);
  });
},

overview: function(data)
    {
        $('#medicatiorequest_overview').empty();
        $('#med-popup-loader').hide();
        
        var query = "SELECT * FROM tblMedication LEFT JOIN tblRequestList ON tblRequestList.listID = tblMedication.MedicationId WHERE tblMedication.AllowToOrder = 'true'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is geen medicatie beschikbaar dat u kunt aanvragen bij uw arts", function(){}, "Melding", "OK");
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                      item = dataset.item(i);
                          
                      $('#medicatiorequest_overview').append('<section class="medicationList" id="med_'+item['MedicationId']+'"><div class="medicationRequestTop"><div class="title">'+item['MedicationName']+'</div></div><div class="medicationRequestBottom"><div class="message">1 x '+item['AmountPerRepetition']+' '+item['Unity']+'</div><div id="cart_'+item['MedicationId']+'" class="cart request"><span onclick="request.addtocart(\''+item['MedicationId']+'\')"><i id="carticon_'+item['MedicationId']+'" class="fa fa-cart-arrow-down"></i> Aanvragen</span></div><div class="clearer"></div></div></section>');
                      
                      if(item['listID'] != null)
                      {
                        $("#cart_"+item['MedicationId']+"").toggleClass("request added");
                        $("#cart_"+item['MedicationId']+"").empty();
                        $("#cart_"+item['MedicationId']+"").append('<span onclick="request.removefromcart('+item['MedicationId']+')"><i class="fa fa-check"></i> Toegevoegd</span>');  
                      }  
                        
                    }
                    
                    request.requestList();
                    $.mobile.changePage("#medicationrequest-page", { transition: "slide", changeHash: true });
                }
            });
        });
    },
    
addtocart: function(id)
    {
        var insertMedication = "INSERT INTO tblRequestList (listID, medicationName, orderAmount) VALUES (?,?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertMedication,[''+id+'', '', '1'],dbase.succesCB,dbase.errorCB);
        });

        $("#cart_"+id+"").toggleClass("request added");
        $("#cart_"+id+"").empty();
        $("#cart_"+id+"").append('<span onclick="request.removefromcart(\''+id+'\')"><i class="fa fa-check"></i> Toegevoegd</span>');
        
        request.requestList();
    },
removefromcart: function(id)
    {
        var deleteMedication = "DELETE FROM tblRequestList WHERE listID = '"+id+"'";
        db.transaction(function(tx, results)
        {
            tx.executeSql(deleteMedication,[],dbase.succesCB,dbase.errorCB);
        });
        
        $("#cart_"+id+"").toggleClass("added request");
        $("#cart_"+id+"").empty();
        $("#cart_"+id+"").append('<span onclick="request.addtocart(\''+id+'\')"><i class="fa fa-cart-arrow-down"></i> Aanvragen</span>');
    
        request.requestList();
    },
    
requestList: function()
    {
        $("#requestnumber").empty();
        var query = "SELECT * FROM tblRequestList";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                $("#requestnumber").append(dataset.length);
            });
        });
    },
    
details: function(status)
    {
        $("#requestlistOverview").empty();
        if(status == 'open')
        {
            var query = "SELECT * FROM tblRequestList LEFT JOIN tblMedication ON tblMedication.MedicationId = tblRequestList.listID";
            
            db.transaction(function(tx)
            {
                tx.executeSql(query,[],function(tx, result)
                {
                    dataset = result.rows;
                    if(dataset.length == 0)
                    {
                        $.mobile.changePage("#medicationrequest-page", { transition: "slidedown", changeHash: false });
                        request.overview();
                        navigator.notification.alert("U heeft nog geen medicatie toegevoegd. Klik op aanvragen om medicatie toe te voegen aan de aanvraaglijst", function(){}, "Melding", "OK");
                    }
                    else
                    {
                        for (var i = 0, item =null; i < dataset.length; i++)
                        {
                            item = dataset.item(i);
                            $("#requestlistOverview").append('<div id="amount_'+item['listID']+'"><div class="title">'+item['MedicationName']+'</div><div class="static_amount"><div class="information">'+item['orderAmount']+'x '+item['AmountPerRepetition']+' '+item['Unity']+'</div></div><div class="actions" onclick="request.remove(\''+item['listID']+'\')"><i class="fa fa-trash"></i></div><div class="clearer"></div></div><hr />');
                              $("#requestlistOverview").trigger( "create" )
                        }
                              
                        $.mobile.changePage("#requestlist-page", { transition: "slideup", changeHash: true });
                    }
                });
            });
        }
        if(status == 'close')
        {
           $.mobile.changePage("#medicationrequest-page", { transition: "slidedown", changeHash: true });

           request.overview(); 
        }
    },
    
changeAmount: function(id)
    {
        $("#amount_"+id+" .static_amount").hide();
        $("#amount_"+id+" .change_amount").fadeIn( "slow", "linear" );
    },
    
updateAmount: function(id)
    {
        $("#amount_"+id+" .change_amount").hide();
        $("#amount_"+id+" .static_amount").fadeIn( "slow", "linear" );
        
        var newAmount = document.getElementById('orderamount_'+id+'');
        
        var updateAmount = "UPDATE tblRequestList SET orderAmount = '"+newAmount.value+"' WHERE listID = '"+id+"'";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(updateAmount,[],dbase.succesCB,dbase.errorCB);
        });
        
        request.details('open');
    },
    
remove: function(id)
    {
        var deleteFromList = "DELETE FROM tblRequestList WHERE listID = '"+id+"'";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(deleteFromList,[],dbase.succesCB,dbase.errorCB);
            request.details('open');
        });
    },

sendrequest: function()
    {
        /*
        var clearList = "DELETE FROM tblRequestList";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(clearList,[],dbase.succesCB,dbase.errorCB);
            
        });
        
        $.mobile.changePage("#medicationrequest-page", { transition: "slidedown", changeHash: false });
        $('#request_confirmation').show();
        $('#request_confirmation').delay(3000).fadeOut('slow');
        
        request.overview();
        */
    }
};

var general = {
showinfo: function(id)
    {
        $( "#"+id+"").slideToggle( "slow" );
    },
};
//capsulating for econsult
var consult = {

showemployees: function(data)
    {
        $("#employees_list").empty();
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            
            $("#employees_list").append('<section class="employee_wrapper" onclick="consult.askquestion(\''+item["EmployeeId"]+'\', \''+item["Name"]+'\')"><div class="image"><i class="fa fa-user fa-3x"></i></div><div class="description"><strong>'+item["Name"]+'</strong></div><div class="chevron"><i class="fa fa-chevron-right"></i></div><div class="clearer"></div></section>');
            
        }
    },
    
askquestion: function(id,name)
    {
        $('#consultemployee').val(''+name+'');
        $('#consultemployeeid').val(''+id+'');
        
        $("#consult_medewerker").empty();
        $.mobile.changePage("#askquestion-page", { transition: "slide", changeHash: true });
        $("#consult_medewerker").append('<div class="thumb"><i class="fa fa-user fa-3x"></div><div class="name"></i><strong>Vraag voor:</strong><br />'+name+'</div><div class="clearer"></div>');
    },
    
validatefields: function()
    {
        if(consultsubject.value == '' || consultToelichtingArea.value == "")
        {
            navigator.notification.alert("U dient alle velden in te vullen", function(){}, "Melding", "OK");
        }
        else
        {
            $("#sendconsult-loader").show();
            rest.getToken('postconsult');
        }
    },

overview: function(data)
    {
        $("#open_container").empty();
        $("#unread_container").empty();
        $("#closed_container").empty();
        
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            
            switch (item["Status"])
            {
                case 0:
                    var status = "open";
                    $("#"+status+"_consults").show();
                    break;
                case 1:
                    var status = "unread";
                    if(item["Unread"] == false)
                    {
                        var status = "closed";
                    }
                    $("#"+status+"_consults").show();
                    break;
            }
            consult.getcontent(status,item);
        }
        $.mobile.changePage("#consultoverview-page", { transition: "slide", changeHash: true });
        $('#question-popup-loader').hide();
    },

getcontent: function(status,item)
    {
        var startsentence = 'Beantwoord door';
        var d = new Date(item["ResponseDate"]);
        
        if(status == "open")
        {
            var startsentence = 'Gesteld aan';
            var d = new Date(item["RequestDate"]);
        }
        
        var day = d.getDate();
        var month = d.getMonth()+1;
        var year = d.getFullYear();
        var dmonth = ''+month+'';
        
        switch (dmonth)
        {
            case "1":
                var month = 'januari';
                break;
            case "2":
                var month = 'februari';
                break;
            case "3":
                var month = 'maart';
                break;
            case "4":
                var month = 'april';
                break;
            case "5":
                var month = 'mei';
                break;
            case "6":
                var month = 'juni';
                break;
            case "7":
                var month = 'juli';
                break;
            case "8":
                var month = 'augustus';
                break;
            case "9":
                var month = 'september';
                break;
            case "10":
                var month = 'oktober';
                break;
            case "11":
                var month = 'november';
                break;
            case "12":
                var month = 'december';
                break;
        }
        $('#consultreferer').val('consultoverview-page');
        $("#"+status+"_container").append('<section class="'+status+'" id="consultid_'+item['ConsultId']+'" onclick="consult.setid(\''+item['ConsultId']+'\')"><div class="'+status+'_wrapper"><div class="subject">'+item['Subject']+'</div><div class="message">'+startsentence+' '+item["Employee"]['Name']+' op '+day+' '+month+' '+year+'</div></div><div class="chevron"><i class="fa fa-chevron-right"></i></div><div class="clearer"></div></section>');
        $("#"+status+"_container").trigger( "create" );
            },
setid: function(id)
    {
        $('#retrieveID').val(id);
        rest.getToken("consultdetails");
    },

convertdate: function(date)
    {
        
        var weekdays = new Array(7);
        weekdays[0] = "Zondag";
        weekdays[1] = "Maandag";
        weekdays[2] = "Dinsdag";
        weekdays[3] = "Woensdag";
        weekdays[4] = "Donderdag";
        weekdays[5] = "Vrijdag";
        weekdays[6] = "Zaterdag";
        
        var d = new Date(date);
        
        var dayofweek = d.getDay();
        var day = d.getDate();
        var month = d.getMonth()+1;
        var year = d.getFullYear();
        var dmonth = ''+month+'';
        
        switch (dmonth)
        {
            case "1":
                var month = 'januari';
                break;
            case "2":
                var month = 'februari';
                break;
            case "3":
                var month = 'maart';
                break;
            case "4":
                var month = 'april';
                break;
            case "5":
                var month = 'mei';
                break;
            case "6":
                var month = 'juni';
                break;
            case "7":
                var month = 'juli';
                break;
            case "8":
                var month = 'augustus';
                break;
            case "9":
                var month = 'september';
                break;
            case "10":
                var month = 'oktober';
                break;
            case "11":
                var month = 'november';
                break;
            case "12":
                var month = 'december';
                break;
        }
        
        var datum = weekdays[dayofweek]+' '+day+' '+month+' '+year;
        return datum;
    },
    
details: function(data)
    {
        $("#question_details").empty();
        $("#answer_details").empty();
    
        var referer = document.getElementById('consultreferer').value;
        $("#consultdetails-page-slider-url").attr("href", "#"+referer+"");
    
        if(data["Status"] == 0)
        {
            var answerSubject = '';
            var answer = '<strong>'+data['Employee']['Name']+'</strong> heeft uw vraag nog niet beantwoord';
            var requestdate = consult.convertdate(data["RequestDate"]);
            var responsedate = '';
        }
        else
        {
            var answerSubject = 'Antwoord van '+data['Employee']['Name']+'';
            var answer = data['Response'];
            var requestdate = consult.convertdate(data["RequestDate"]);
            var responsedate = consult.convertdate(data["ResponseDate"]);
        }
        
        $("#question_details").append('<div class="subject">'+data['Subject']+'</div><div class="message">'+data['Text']+'</div><div class="date">'+requestdate+'</div>');
        $("#question_details").trigger( "create" );
        
        $("#answer_details").append('<div class="subject">'+answerSubject+'</div><div class="message">'+answer+'</div><div class="date">'+responsedate+'</div>');
        $("#answer_details").trigger( "create" );
        
        $.mobile.changePage("#consultdetails-page", { transition: "slideup", changeHash: true });
        $('#consult-loader').hide();
    },

    
toggle: function(id)
    {
        $( "#"+id+"_container").slideToggle( "slow" );
        $("#"+id+"_chevron").toggleClass("fa-chevron-right fa-chevron-down");
    },

};

var agenda = {
    
selection: function(data)
    {
        //alert(JSON.stringify(data));
        $('#agenda_selection').empty();
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            
            $('#agenda_selection').append('<section class="employee_wrapper" onclick="agenda.suggestion(\''+item['AgendaId']+'\',\''+item['AgendaName']+'\')"><div class="image"><i class="fa fa-user fa-3x"></i></div><div class="description"><strong>'+item['AgendaName']+'</strong><br /><small>Beschikbaar voor afspraken</small></div><div class="chevron"><i class="fa fa-chevron-right"></i></div><div class="clearer"></div></section>');
        
        }
    },

suggestion: function(agendaid,agendaname)
    {
        $('#selectagenda-loader').show();
        
        rest.getToken('getclosestdate');
        
        $('#agendachoise').val(''+agendaid+'');
        $("#agenda_medewerker").empty();
        $("#finalize_medewerker").empty();
        
        $("#agenda_medewerker").append('<div class="thumb"><i class="fa fa-user fa-3x"></div><div class="name"></i><strong>Afspraak maken in:</strong><br />'+agendaname+'</div><div class="clearer"></div>');
         $("#finalize_medewerker").append(' '+agendaname+'');
        
        
    },

closestdate: function(data)
    {
        var weekdays = new Array(7);
        weekdays[0] = "Zondag";
        weekdays[1] = "Maandag";
        weekdays[2] = "Dinsdag";
        weekdays[3] = "Woensdag";
        weekdays[4] = "Donderdag";
        weekdays[5] = "Vrijdag";
        weekdays[6] = "Zaterdag";
        
        $('#agendaTypeName').val(''+data['AgendaTypes'][0]['Name']+'');//store agendatype name for later use
        
        var date = new Date(data['AgendaTypes'][0]['Dates'][0]);
        dayofweek = date.getDay();
        date = date.toLocaleDateString()
        
        $('#appointmentdate').val(data['AgendaTypes'][0]['Dates'][0]);
        $('#nearestdate').empty();
        $('#nearestdate').append(weekdays[dayofweek]+' '+date);
        rest.getToken('getnearesttimeblock');
    },

nearestTime:function(data)
    {
        var startdate = data[0]["FromDateTime"];
        var startarray = startdate.split("T");
        var starttime = startarray[1].split(":");
        
        var enddate = data[0]["ToDateTime"];
        var endarray = enddate.split("T");
        var endtime = endarray[1].split(":");
        $('#appointmenttime').val(starttime[0]+':'+starttime[1]+' - '+endtime[0]+':'+endtime[1]);
        $('#appointmenttimeslotid').val(data[0]["TimeSlotId"]);
        
        
        $('#nearestdatetime').append('van '+starttime[0]+':'+starttime[1]+' tot '+endtime[0]+':'+endtime[1]);
        
        $('#selectagenda-loader').hide();
        $.mobile.changePage("#appointmentsuggestion_page", { transition: "slide", changeHash: true });
    },
    
finalize: function()
    {
        //$('#appointmentdate').val('24-3-2016');
        //$('#appointmenttime').val('10:15 - 10:20');
        //$("#requested_datetime").empty();
        
        //$("#requested_datetime").append('Donderdag 24 maart<br /><small>10:15 - 10:20</small>');
        var time = document.getElementById('appointmenttime');
        var date = document.getElementById('appointmentdate');
        
        var weekdays = new Array(7);
        weekdays[0] = "Zondag";
        weekdays[1] = "Maandag";
        weekdays[2] = "Dinsdag";
        weekdays[3] = "Woensdag";
        weekdays[4] = "Donderdag";
        weekdays[5] = "Vrijdag";
        weekdays[6] = "Zaterdag";
        
        var date = new Date(date.value);
        dayofweek = date.getDay();
        date = date.toLocaleDateString()
        
        $("#requested_datetime").empty();
        $("#requested_datetime").append(weekdays[dayofweek]+' '+date+'<br /><small>'+time.value+'</small>');
        
        $.mobile.changePage("#appointmentfinalize_page", { transition: "slide", changeHash: true });
    },
    
changetime: function(data)
    {
        //alert(JSON.stringify(data));
        
        var page = document.getElementById("appointmentrefer");
        
        $("#appointmentchangetime_page-slider-url").attr("href", "#"+page.value+"");
        $('#selectedDateStrong').empty();
        var selecteddate = document.getElementById("appointmentdate");
        
        var weekdays = new Array(7);
        weekdays[0] = "zondag";
        weekdays[1] = "maandag";
        weekdays[2] = "dinsdag";
        weekdays[3] = "woensdag";
        weekdays[4] = "donderdag";
        weekdays[5] = "vrijdag";
        weekdays[6] = "zaterdag";
        
        var d = new Date(selecteddate.value),
        
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        
        dayofweek = d.getDay();
        
        //if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        
        switch (month)
        {
            case "1":
                var month = 'januari';
                break;
            case "2":
                var month = 'februari';
                break;
            case "3":
                var month = 'maart';
                break;
            case "4":
                var month = 'april';
                break;
            case "5":
                var month = 'mei';
                break;
            case "6":
                var month = 'juni';
                break;
            case "7":
                var month = 'juli';
                break;
            case "8":
                var month = 'augustus';
                break;
            case "9":
                var month = 'september';
                break;
            case "10":
                var month = 'oktober';
                break;
            case "11":
                var month = 'november';
                break;
            case "12":
                var month = 'december';
                break;
        }
        var datum = [day, month, year].join(' ');
        
        $('#selectedDateStrong').append(weekdays[dayofweek]+' '+datum);
        
        $('#changetime').empty();
        
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            //alert(item["TimeSlotId"]);
            var starttime = data[i]["FromDateTime"];
            var starttimearray = starttime.split("T");
            var starttime = starttimearray[1].split(":");
            
            var endtime = data[i]["ToDateTime"];
            var endtimearray = endtime.split("T");
            var endtime = endtimearray[1].split(":");
            
        $('#changetime').append('<div class="timeslot" onclick="agenda.saveNewDateTime(\''+starttime[0]+':'+starttime[1]+' - '+endtime[0]+':'+endtime[1]+'\',\''+item['TimeSlotId']+'\');">'+starttime[0]+':'+starttime[1]+'<br /><small>'+endtime[0]+':'+endtime[1]+'</small></div>');
            
        }
        $('#changetime').append('<div class="clearer"></div>');
        
        $.mobile.changePage("#appointmentchangetime_page", { transition: "slide", changeHash: true });
        $('#agenda-loader').hide();
    },

changeday: function()
    {
        $.mobile.changePage("#appointmentchangedate_page", { transition: "slide", changeHash: true });
        //agenda.updatecalendar();
    },
test: function()
    {
        //alert('aap');
    },

showAmmountFreeSlots: function(date)
    {
        var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        
        var datum = [year, month, day].join('-');
        
        
        $("#calenderActiveDay").val(datum);
        var count = rest.getToken("countAvailableSlots");
        
    },
    
updatecalendar: function(data)
    {
        
        var datums = data["AgendaTypes"][0]["Dates"];
        arr = [];
        
        for (var i = 0, item =null; i < datums.length; i++)
        {
            item = datums[i];
            
            arr.push({ "summary" : "Test event",
                     "begin" : new Date(item),
                     "end" : new Date(item),
                     "allDayTimeString" : "afspraak mogelijkheid vanaf 09:00"});
        }
        
       
        $("#calendar").trigger('refresh');
        /*
        $("#calendar").bind('change', function(event, date)
        {
            alert(date);
        });
        */
        $.mobile.changePage("#appointmentchangedate_page", { transition: "slide", changeHash: true });
        $('#agenda-loader').hide();
    },

updateday: function(date)
    {
      var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
      
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      
      var datum = [year, month, day].join('-');
      $('#appointmentdate').val(''+datum+'');
      $('#appointmentrefer').val('appointmentchangedate_page');
      
      //$.mobile.changePage("#appointmentchangetime_page", { transition: "slide", changeHash: false });
      rest.getToken('getTimeSlots');
    },

saveNewDateTime: function(time,timeslotid)
    {
        //alert(timeslotid);
        $('#appointmenttime').val(time);
        $('#appointmenttimeslotid').val(timeslotid);
        var date = document.getElementById('appointmentdate');
        
        var weekdays = new Array(7);
        weekdays[0] = "Zondag";
        weekdays[1] = "Maandag";
        weekdays[2] = "Dinsdag";
        weekdays[3] = "Woensdag";
        weekdays[4] = "Donderdag";
        weekdays[5] = "Vrijdag";
        weekdays[6] = "Zaterdag";
        
        var date = new Date(date.value);
        dayofweek = date.getDay();
        date = date.toLocaleDateString();
        
        $("#requested_datetime").empty();
        $("#requested_datetime").append(weekdays[dayofweek]+' '+date+'<br /><small>'+time+'</small>');
        $.mobile.changePage("#appointmentfinalize_page", { transition: "slide", changeHash: true });
    },

save: function(data)
    {
        //alert('IVM de mockup wordt deze afspraak (nog) niet daadwerkelijk opgeslagen. Wachten op website/his systeem op de acc');
        $('#afspraak-loader').hide();
        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
        
        /*
        var professional = document.getElementById('agendaemployee');
        var date = document.getElementById('appointmentdate');
        var time = document.getElementById('appointmenttime');
        var dag = document.getElementById('agendadag');
        var description = document.getElementById('agendaToelichtingArea');
        
        var agendaID = (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        
        var insertAppointment = "INSERT INTO tblAppointments (appointmentID, date, time, day, professional, reason, status) VALUES (?,?,?,?,?,?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertAppointment,[''+agendaID+'', ''+date.value+'', ''+time.value+'', ''+dag.value+'', ''+professional.value+'',''+description.value+'','open'],dbase.succesCB,dbase.errorCB);
                       
        });
        
        dashboard.consults();
        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });
         */
    },
    
overview: function(data)
    {
        //alert(JSON.stringify(data));
        $("#open_appointments").hide();
        $("#closed_appointments").hide();
        $("#open_agendacontainer").empty();
        $("#closed_agendacontainer").empty();
        //alert(JSON.stringify(data));
        currentdate = new Date();
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            
            var date = new Date(item['StartDateTime']);
            
            //var offset = date.getTimezoneOffset() / 60;
            //var hours = date.getHours();
            //date.setHours(hours - offset);
            var datum = date.toLocaleDateString();
            var time = date.toLocaleTimeString();

            if(currentdate >= date )
            {
                $("#closed_appointments").show();
                agenda.getcontent('closed',item);
            }
            else
            {
                $("#open_appointments").show();
                agenda.getcontent('open',item);
            }
        }
        
        $.mobile.changePage("#appointmentoverview-page", { transition: "slide", changeHash: true });
        $('#agenda-popup-loader').hide();
    },
    
getcontent: function(status, item)
    {
        var startdate = new Date(item['StartDateTime']);
        //var enddate = new Date(item['EndDateTime']);
        var datum = startdate.toLocaleDateString();
        //var starttime = startdate.toLocaleTimeString();
                
        var starttime = item['StartDateTime'];
        var starttimearray = starttime.split("T");
        var starttime = starttimearray[1].split(":");

        var endtime = item['EndDateTime'];
        var endtimearray = endtime.split("T");
        var endtime = endtimearray[1].split(":");
        
        var weekdays = new Array(7);
        weekdays[0] = "Zondag";
        weekdays[1] = "Maandag";
        weekdays[2] = "Dinsdag";
        weekdays[3] = "Woensdag";
        weekdays[4] = "Donderdag";
        weekdays[5] = "Vrijdag";
        weekdays[6] = "Zaterdag";
        
        dayofweek = startdate.getDay();
        
        $("#"+status+"_agendacontainer").append('<section class="'+status+'" id="appointmentid_'+item['AppointmentId']+'" onclick="agenda.details(\''+item['AppointmentId']+'\', \'appointmentoverview-page\')"><div class="'+status+'_wrapper"><div class="subject">'+weekdays[dayofweek]+' '+datum+'</div><div class="message">Van '+starttime[0]+':'+starttime[1]+' tot '+endtime[0]+':'+endtime[1]+'</div></div><div class="chevron"><i class="fa fa-chevron-right"></i></div><div class="clearer"></div></section>');
        $("#"+status+"_agendacontainer").trigger( "create" );
        
    },
    
toggle: function(id)
    {
        $( "#"+id+"_agendacontainer").slideToggle( "slow" );
        $("#"+id+"_agendachevron").toggleClass("fa-chevron-right fa-chevron-down");
    },
    
details: function(id,page)
    {
        $("#appointment_returner").attr("href", "#"+page+"");
        $("#appointmentdetails").empty();
        $('#details_appointmentid').val(id);
        $('#dashboard-loader').show();
        rest.getToken('getappointmentdetails');
        
        
    },
    
showdetails:function(data)
    {
        $("#appointment-details-loader").hide();
        $('#dashboard-loader').hide();
        $('#details_timeslotid').val(data['TimeSlotId']);
        $('#details_description').val(data['Description']);
        $('#details_startdatetime').val(data['StartDateTime']);
        $('#details_enddatetime').val(data['EndDateTime']);
        $('#details_agendaid').val(data['AgendaId']);
       
        var startdate = new Date(data['StartDateTime']);
        var enddate = new Date(data['EndDateTime']);
        var datum = startdate.toLocaleDateString();
        
        var starttime = data['StartDateTime'];
        var starttimearray = starttime.split("T");
        var starttime = starttimearray[1].split(":");

        var endtime = data['EndDateTime'];
        var endtimearray = endtime.split("T");
        var endtime = endtimearray[1].split(":");
        
        var weekdays = new Array(7);
        weekdays[0] = "Zondag";
        weekdays[1] = "Maandag";
        weekdays[2] = "Dinsdag";
        weekdays[3] = "Woensdag";
        weekdays[4] = "Donderdag";
        weekdays[5] = "Vrijdag";
        weekdays[6] = "Zaterdag";
        
        dayofweek = startdate.getDay();
                
        $('#appointmentdetails').append('<div class="icon"><i class="fa fa-clock-o fa-2x"></i></div><div class="title" id="requested_datetime">'+weekdays[dayofweek]+' '+datum+'<br /><small>van '+starttime[0]+':'+starttime[1]+' tot '+endtime[0]+':'+endtime[1]+'</small></div><div id="appointment_remove" onclick="rest.getToken(\'removeAppointment\');">Annuleren</div><div class="clearer"></div><br /><div class="icon"><i class="fa fa-tag fa-2x"></i></div><div class="title"><strong>Reden voor afspraak:</strong><br />'+data["Description"]+'</div><div class="clearer"></div>');
        
        if(data['CanBeModified'] == true)
        {
            $('#appointment_remove').show();
        }
        $.mobile.changePage("#appointmentdetails-page", { transition: "slide", changeHash: false });
    },
};

var dashboard = {

appointments:function(data)
    {
        for (var i = 0, item =null; i < data.length; i++)
        {
            item = data[i];
            
            var startdate = new Date(item['StartDateTime']);
            var datum = startdate.toLocaleDateString();
                    
            var starttime = item['StartDateTime'];
            var starttimearray = starttime.split("T");
            var starttime = starttimearray[1].split(":");

            dayofweek = startdate.getDay();
            
            var currentdate = new Date();
            var date = new Date(item['StartDateTime']);
            if(startdate >= currentdate )
            {
              $('#message_center').append('<div class="content" onclick="agenda.details(\''+item['AppointmentId']+'\', \'dashboard-page\')"><div class="icon"><i class="fa fa-bell-o fa-2x"></i></div><div class="message"><strong>Afspraak ingepland</strong><br />'+weekdays[dayofweek]+' '+datum+' om '+starttime[0]+':'+starttime[1]+'</div><div class="clearer"></div></div>');
              //$('#message_center').show( "slidedown" );
              showMessageCenter = true;  
            }
        }

        $('#dashboard-loader').hide();
        dashboard.messageCenter();
    },
    
consults:function(data)
    {
      $('#message_center').empty();
      
      for (var i = 0, item =null; i < data.length; i++)
      {
        item = data[i];
        
        if(item["Status"] == 0)
        {
          $('#message_center').append('<div class="content" onclick="rest.getToken(\'consultoverview\')"><div class="icon"><i class="fa fa-bell-o fa-2x"></i></div><div class="message"><strong>Openstaande vraag</strong><br />Wacht op antwoord van '+item['Employee']["Name"]+'</div><div class="clearer"></div></div>');  
        
          //$('#message_center').show( "slidedown" );
          showMessageCenter = true;
        }
        if(item["Unread"] == true)
        {
          $('#message_center').append('<div class="content" onclick="rest.getToken(\'consultoverview\')"><div class="icon"><i class="fa fa-bell-o fa-2x"></i></div><div class="message"><strong>Uw vraag is beantwoord </strong><br />Lees het antwoord van '+item['Employee']["Name"]+'</div><div class="clearer"></div></div>');
          //$('#message_center').show( "slidedown" );
          showMessageCenter = true;
        }
      }
      dashboard.messageCenter();
    },
  messageCenter:function()
  {
    if(showMessageCenter == true)
    {
      //alert("showMessageCenter");
      $('#message_center').slideDown();
    }
    else
    {
      //alert('fail');
      $('#message_center').hide();
    }
  }

};

var feedback = {
  verify:function()
  {
    var feedback_textarea = document.getElementById("feedback_textarea").value;
    var feedback_name = document.getElementById("feedback_name").value;
    var feedback_phonenumber = document.getElementById("feedback_phonenumber").value;
    var feedback_email = document.getElementById("feedback_email").value;
    
        
    if (feedback_textarea ==""  )
    {
      navigator.notification.alert("U heeft nog geen opmerking of suggestie voor de Medi App ingevuld", function(){}, "Melding", "OK");
      return;
    }
    else
    {
      //alert(selectedCustomerId);
      $.ajax({
        url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
        type: 'POST',
        data: { feedback:selectedCustomerId, suggestion:feedback_textarea, name:feedback_name, phonenumber:feedback_phonenumber, email:feedback_email},
        success: function(data) {
          $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });
          navigator.notification.alert("Bedankt voor uw opmerking en/of suggestie", function(){}, "Verzonden", "OK");
        },
        error: function()
        {
        navigator.notification.alert("Er is een onverwachte fout opgetreden bij het versturen van uw feedback.", function(){}, "Helaas", "OK");
        }
        });
    }
  }
};

var statistics = {

  register:function(action,statuscode)
  {
    var devicetype = device.model;

    $.ajax({
     url: 'https://www.pharmeon-socialmedia.nl/medi-app/callback.php',
     type: 'POST',
     data: { stats:statuscode, appid:slavename, devicetype:devicetype, customerId:selectedCustomerId, action:action},
     success: function(data) {
      //alert(data);
     },
     error: function()
     {
      //navigator.notification.alert("Er is een onverwachte fout opgetreden bij het ophalen van de beschikbare huisartsen.", function(){}, "Helaas", "OK");
     }
     });
  },

};

